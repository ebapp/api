from flask import url_for, json
from api.dbs import likes

headers = {
    'Content-Type': 'application/json'
}


def test_like_without_key(client, test_text, dentist_test, collection):
    invalid_key_data = dict(key='churrasco',
                            state='any',
                            text_id=str(test_text['_id']),
                            dentist_id=str(dentist_test['_id']))
    response = client.post(url_for('routes.like'), data=json.dumps(invalid_key_data), headers=headers)
    response_obj = json.loads(response.data)
    query = {'text_id': test_text['_id'], 'dentist_id': dentist_test['_id']}
    count = collection.like_col.count_documents(query)
    assert response_obj['error'] == 'unauthorized'
    assert count == 0


def test_invalid_state(app, client, test_text, dentist_test, collection):
    valid_key_data = dict(key=app.config['SECRET_KEY'],
                          state='caneta',
                          text_id=str(test_text['_id']),
                          dentist_id=str(dentist_test['_id']))
    response = client.post(url_for('routes.like'), data=json.dumps(valid_key_data), headers=headers)
    response_obj = json.loads(response.data)
    query = {'text_id': test_text['_id'], 'dentist_id': dentist_test['_id']}
    count = collection.like_col.count_documents(query)
    assert response_obj['error'] == 'state must be like, dislike or remove'
    assert count == 0


def test_giving_like(client, app, test_text, dentist_test, collection):
    valid_key_data = dict(key=app.config['SECRET_KEY'],
                          state='like',
                          text_id=str(test_text['_id']),
                          dentist_id=str(dentist_test['_id']))
    response = client.post(url_for('routes.like'), data=json.dumps(valid_key_data), headers=headers)
    response_obj = json.loads(response.data)

    query = {'state': 'like', 'text_id': test_text['_id'], 'dentist_id': dentist_test['_id']}
    count = collection.like_col.count_documents(query)
    inserted = collection.like_col.find_one(query)
    collection.like_col.delete_one({'_id': inserted['_id']})

    assert response_obj['status'] == 'like'
    assert response_obj['likes'] == 1
    assert response_obj['dislikes'] == 0
    assert count == 1


def test_giving_dislike(client, app, test_text, dentist_test, collection):
    valid_key_data = dict(key=app.config['SECRET_KEY'],
                          state='dislike',
                          text_id=str(test_text['_id']),
                          dentist_id=str(dentist_test['_id']))
    response = client.post(url_for('routes.like'), data=json.dumps(valid_key_data), headers=headers)
    response_obj = json.loads(response.data)

    query = {'state': 'dislike', 'text_id': test_text['_id'], 'dentist_id': dentist_test['_id']}
    count = collection.like_col.count_documents(query)
    inserted = collection.like_col.find_one(query)
    collection.like_col.delete_one({'_id': inserted['_id']})

    assert response_obj['status'] == 'dislike'
    assert response_obj['likes'] == 0
    assert response_obj['dislikes'] == 1
    assert count == 1

def test_removing_like(client, app, like_test, collection):
    valid_key_data = dict(key=app.config['SECRET_KEY'],
                          state='remove',
                          text_id=str(like_test['text_id']),
                          dentist_id=str(like_test['dentist_id']))
    response = client.post(url_for('routes.like'), data=json.dumps(valid_key_data), headers=headers)
    response_obj = json.loads(response.data)

    query = {'text_id': like_test['text_id'], 'dentist_id': like_test['dentist_id']}
    count = collection.like_col.count_documents(query)

    assert response_obj['status'] == 'remove'
    assert response_obj['likes'] == 0
    assert response_obj['dislikes'] == 0
    assert count == 0

def test_removing_dislike(client, app, dislike_test, collection):
    valid_key_data = dict(key=app.config['SECRET_KEY'],
                          state='remove',
                          text_id=str(dislike_test['text_id']),
                          dentist_id=str(dislike_test['dentist_id']))
    response = client.post(url_for('routes.like'), data=json.dumps(valid_key_data), headers=headers)
    response_obj = json.loads(response.data)

    query = {'text_id': dislike_test['text_id'], 'dentist_id': dislike_test['dentist_id']}
    count = collection.like_col.count_documents(query)

    assert response_obj['status'] == 'remove'
    assert response_obj['likes'] == 0
    assert response_obj['dislikes'] == 0
    assert count == 0

def test_change_like_to_dislike(client, app, like_test, collection):
    valid_key_data = dict(key=app.config['SECRET_KEY'],
                          state='dislike',
                          text_id=str(like_test['text_id']),
                          dentist_id=str(like_test['dentist_id']))
    response = client.post(url_for('routes.like'), data=json.dumps(valid_key_data), headers=headers)
    response_obj = json.loads(response.data)

    query = {'text_id': like_test['text_id'], 'dentist_id': like_test['dentist_id']}
    count = collection.like_col.count_documents(query)
    inserted = collection.like_col.find_one(query)
    collection.like_col.delete_one({'_id': inserted['_id']})

    assert response_obj['status'] == 'dislike'
    assert count == 1
    assert inserted['state'] == 'dislike'
    assert response_obj['likes'] == 0
    assert response_obj['dislikes'] == 1

def test_change_dislike_to_like(client, app, dislike_test, collection):
    valid_key_data = dict(key=app.config['SECRET_KEY'],
                          state='like',
                          text_id=str(dislike_test['text_id']),
                          dentist_id=str(dislike_test['dentist_id']))
    response = client.post(url_for('routes.like'), data=json.dumps(valid_key_data), headers=headers)
    response_obj = json.loads(response.data)

    query = {'text_id': dislike_test['text_id'], 'dentist_id': dislike_test['dentist_id']}
    count = collection.like_col.count_documents(query)
    inserted = collection.like_col.find_one(query)
    collection.like_col.delete_one({'_id': inserted['_id']})

    assert response_obj['status'] == 'like'
    assert count == 1
    assert inserted['state'] == 'like'
    assert response_obj['likes'] == 1
    assert response_obj['dislikes'] == 0
