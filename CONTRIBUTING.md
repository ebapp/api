# How to contribute

First of all, we're glad for receiving you as a contributor in our repository. When contributing code, you will need to follow three steps:

1. Fork the repository on GitLab:
> git clone https://gitlab.com/ebapp/api

2. Run the tests to confirm they all pass on your system. If they don’t, you’ll need to investigate the failure yourself or ask for help from your teammates.

3. Submit your changes:

> git add changed_file.py

> git commit -m "The reason why you are submitting the pull-request"

> git push -u origin master

Consider discussing with your team the changes you have made and why they are important for the project. 
We appreciate the use of best pratices in code writting, using guidelines as <a href="https://www.python.org/dev/peps/pep-0008/">PEP8</a>.


# Tests
Run all tests:
```
pytest
```


# Libraries
This project has the following libraries which are included in requirements.txt:

#### Flask (Webserver Framework):
> http://flask.pocoo.org/docs/1.0/installation/ <br>

#### Flask-PyMongo (MongoDB Module):
> https://flask-pymongo.readthedocs.io/en/latest/ <br>

#### Flask-PyTest (Tests):
> https://pytest-flask.readthedocs.io/en/latest/ <br>

#### Flask-WTF (Forms):
> https://flask-wtf.readthedocs.io/en/stable/ <br>

#### Flask-CORS (Cross Origin Resource Sharing):
> https://flask-cors.readthedocs.io/en/latest/

# API


Structure of Objects:

#### Methodology Object
```
<methodology_obj> = {
    "_id": { "$oid" : "<object_id>" },
    "name": "<methodology_name>",
    "text": "<methodology_text",
    "authors": ["<author_id>", "<author_id>", ... ],
    "thermometer": <thermometer_value>,
    "last_update_date": "YYYY-MM-DD HH:MM:SS"
}
```

#### Text Object
```
<text_obj> = {
    "_id": { "$oid" : "<object_id>" },
    "title" : "<text_title>",
    "url_image": "<image_address>",
    "abstract": "<text_abstract>",
    "methodologies": ["<methodology_id>", "<methodology_id>", ... ],
    "themes": ["themes_id", "themes_id", ... ],
    "paper_link": "<paper_address>",
    "abstract_authors": ["<author_id>", "<author_id>", ... ],
    "study_relevance": <relevance_value>,
    "posting_date": "YYYY-MM-DD HH:MM:SS",
    "last_update_date": "YYYY-MM-DD HH:MM:SS"
}
```

#### Theme Object
```
<theme_obj> = {
    "_id": { "$oid": "<object_id>" },
    "name": "theme_name",
    "color": "<color_hex>",
    "last_update_date": "YYYY-MM-DD HH:MM:SS"
}
```

#### User Object
```
<user_obj> = {
    "_id": { "$oid": "<object_id>" },
    "name": "<user_name>",
    "email": "<user_email>",
    "admin": <boolean>,
    "about": "<user_about>",
    "password": "<user_password>"
}
```

#### Requests
Make a POST request to this route: `/methodology/detail` passing a JSON with the following format:
```
{
    "key": "SECRET_KEY"
    "_id": "<methodology_id>"
}
```
And receiving the following response:
```
{
    "methodologies": <methodology_obj>,
    "status": <status_number>,
    "message": "<message_text>"
}
```

Make a POST request to this route: `/methodology/selected` passing a JSON with the following format:
```
{
    "key": "SECRET_KEY",
    "methodologies_list": ["<methodology_id>", "<methodology_id>", ... ]>
}
```

And receiving the following response:
```
{
    "methodologies": [ <methodology_obj>, <methodology_obj>, ... ],
    "status": <status_number>,
    "message": "<message_text>"
}
```

Make a POST request to this route: `/text/detail` passing a JSON with the following format:
```
{
    "key": "SECRET_KEY",
    "_id": "<text_id>"
}
```

And receiving the following response:
```
{
    "text": <text_obj>,
    "status": <status_number>,
    "message": "<message_text>"
}
```

Make a POST request to this route: `/text/search` passing a JSON with the following format:
```
{
    "key": "SECRET_KEY",
    "theme": "<theme_id>",
    "keyword": "<text_keyword>"
}
```

And receiving the following response:
```
{
    "texts": [ <text_obj>, <text_obj>, ... ],
    "status": <status_number>,
    "message": "<message_text>"
}
```

Make a POST request to this route: `/themes/list` passing a JSON with the following format:
```
{
    "key": "SECRET_KEY"
}
```

And receiving the following response:
```
{
    "themes": [ <theme_obj>, <theme_obj>, ... ],
    "status": <status_number>,
    "message": "<message_text>"
}
```

Make a POST request to this route: `/users/selected` passing a JSON with the following format:
```
{
    "key": "SECRET_KEY",
    "users_list": [ "<user_id>", "<user_id>", ... ]
}
```

And receiving the following response:
```
{
    "users": [ <user_obj>, <user_obj>, ... ],
    "status": <status_number>,
    "message": "<message_text>"
}
```
