from flask import url_for
import pytest


@pytest.fixture
def data(test_registered_user):
    return dict(name='test_methodology_edited',
                text='test methodology text_edited',
                authors=[str(test_registered_user['_id'])],
                thermometer=1)


def test_login_required(client, test_methodology, data):
    response = client.post(url_for('routes.methodologies_update', methodology_id=str(test_methodology['_id'])),
                           data=data, follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_methodology, test_logged_user):
    response = client.get(url_for('routes.methodologies_update', methodology_id=str(test_methodology['_id'])),
                          follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_methodology, test_logged_user, data):
    response = client.post(url_for('routes.methodologies_update', methodology_id=str(test_methodology['_id'])),
                           data=data, follow_redirects=True)
    assert response.status_code == 200


def test_success_update_data(client, test_logged_user, test_methodology, collection, data):
    response = client.post(url_for('routes.methodologies_update', methodology_id=str(test_methodology['_id'])),
                           data=data, follow_redirects=True)
    methodology_obj = collection.methodologies_col.find_one({'_id': test_methodology['_id']})
    assert b'success' in response.data
    assert methodology_obj['name'] == data['name']
    assert methodology_obj['text'] == data['text']
    assert methodology_obj['authors'] == data['authors']
    assert methodology_obj['thermometer'] == data['thermometer']
    assert not methodology_obj['last_update_date'] == test_methodology['last_update_date']


def test_fail_update_data(client, test_logged_user, test_methodology, collection, data):
    data['name'] = ''
    response = client.post(url_for('routes.methodologies_update', methodology_id=str(test_methodology['_id'])),
                           data=data, follow_redirects=True)
    methodology_obj = collection.methodologies_col.find_one({'_id': test_methodology['_id']})
    assert b'success' not in response.data
    assert not methodology_obj['name'] == data['name']
    assert not methodology_obj['text'] == data['text']
    assert not methodology_obj['authors'] == data['authors']
    assert not methodology_obj['thermometer'] == data['thermometer']
    assert methodology_obj['last_update_date'] == test_methodology['last_update_date']
