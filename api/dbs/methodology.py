from api import mongo
from bson.objectid import ObjectId
import pymongo

collection = mongo.db.methodologies_col 


# insert methodology to database
def insert(obj):
    collection.insert_one(obj)


# update methodology from database
def update(methodology_id, obj):
    collection.find_one_and_update({'_id': ObjectId(methodology_id)}, {'$set': obj})


# delete methodology from database
def delete(methodology_id):
    collection.delete_one({'_id': ObjectId(methodology_id)})


def find_all():
    return list(collection.find().sort("last_update_date", pymongo.DESCENDING))


def find_one(methodology_id):
    methodology = collection.find_one({'_id': ObjectId(methodology_id)})
    return methodology


def find_selected(methodologies_list):
    return collection.find({'_id': {'$in': [ObjectId(methodology_id) for methodology_id in methodologies_list]}})
