from flask import url_for
import pytest
import bcrypt

@pytest.fixture
def data():
    return dict(facebook_id='10',
                google_id = '15',
                name='Ze do raio',
                email='zedoraio@relampago.com',
                email_password = '4455667788',
                birth_year = 1910,
                gender = 'M',
                grade_year = 1967,
                grade_state = 'GO',
                occupation = 'ESTUDANTE',
                work_state = 'GO',
                app_ad = 'ABOPED'
                )

def test_invalid_dentist_data_redirection(client, app, data, collection):
    data['key'] = app.config['SECRET_KEY']
    data['name'] = ''
    response = client.post(url_for('routes.dentists_create'), data=data, follow_redirects=True)
    dentist_obj = collection.dentists_col.find_one(data)
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['erro'] == 'Favor preencher todos os campos.'
    assert dentist_obj == None

def test_dentist_without_key(client, app, data, collection):
    data['key'] = ''
    response = client.post(url_for('routes.dentists_create'), data=data, follow_redirects=True)
    dentist_obj = collection.dentists_col.find_one(data)
    assert response.is_json
    assert dentist_obj == None
    resp_obj = response.get_json()
    assert resp_obj['error'] == 'unauthorized'

def test_dentist_with_wrong_key(client, app, data, collection):
    data['key'] = '123'
    response = client.post(url_for('routes.dentists_create'), data=data, follow_redirects=True)
    dentist_obj = collection.dentists_col.find_one(data)
    assert response.is_json
    assert dentist_obj == None
    resp_obj = response.get_json()
    assert resp_obj['error'] == 'unauthorized'

def test_valid_dentist_data_redirection(client, app, data, collection):
    data['key'] = app.config['SECRET_KEY']
    response = client.post(url_for('routes.dentists_create'), data=data, follow_redirects=True)
    dentist_obj = collection.dentists_col.find_one({'name': data['name']})
    collection.dentists_col.delete_one({'_id': dentist_obj['_id']})
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['name'] == 'Ze do raio'
    assert resp_obj['name'] == dentist_obj['name']
    assert resp_obj['email'] == data['email']
    assert resp_obj['email'] == dentist_obj['email']
    email_password_from_db = bytes(dentist_obj['email_password'], 'UTF-8')
    email_password_from_data = bytes(data['email_password'], 'UTF-8')
    assert bcrypt.checkpw(email_password_from_data, email_password_from_db)
    assert resp_obj['birth_year'] == data['birth_year']
    assert resp_obj['birth_year'] == dentist_obj['birth_year']
    assert resp_obj['gender'] == data['gender']
    assert resp_obj['gender'] == dentist_obj['gender']
    assert resp_obj['grade_year'] == data['grade_year']
    assert resp_obj['grade_year'] == dentist_obj['grade_year']
    assert resp_obj['grade_state'] == data['grade_state']
    assert resp_obj['grade_state'] == dentist_obj['grade_state']
    assert resp_obj['occupation'] == data['occupation']
    assert resp_obj['occupation'] == dentist_obj['occupation']
    assert resp_obj['work_state'] == data['work_state']
    assert resp_obj['work_state'] == dentist_obj['work_state']
    assert resp_obj['app_ad'] == data['app_ad']
    assert resp_obj['app_ad'] == dentist_obj['app_ad']

def test_email_already_exists(client, app, data, collection):
    data['key'] = app.config['SECRET_KEY']
    collection.dentists_col.insert_one(data)
    response = client.post(url_for('routes.dentists_create'), data=data, follow_redirects=True)
    dentist_obj = collection.dentists_col.find_one({'email': data['email']})
    count = collection.dentists_col.count_documents({'email': data['email']})
    collection.dentists_col.delete_one({'_id': dentist_obj['_id']})
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['erro'] == 'Um usuário com este email já esta cadastrado.'
    assert  count == 1

def test_valid_dentist_unrequired_data_redirection(client, app, data, collection):
    data['key'] = app.config['SECRET_KEY']
    data['occupation'] = ''
    data['work_state'] = ''
    data['app_ad'] = ''

    response = client.post(url_for('routes.dentists_create'), data=data, follow_redirects=True)
    dentist_obj = collection.dentists_col.find_one({'name': data['name']})
    collection.dentists_col.delete_one({'_id': dentist_obj['_id']})
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['occupation'] == ''
    assert resp_obj['work_state'] == ''
    assert resp_obj['app_ad'] == ''
    assert resp_obj['name'] == dentist_obj['name']
