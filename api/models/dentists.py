from flask_wtf import FlaskForm
from wtforms import StringField, widgets, IntegerField, DateField
from wtforms.validators import DataRequired

class DentistsForm(FlaskForm):

    class Meta:
        csrf = False

    key = StringField('key', validators=[DataRequired()])
    facebook_id=StringField('facebook_id')
    google_id = StringField('google_id')
    name=StringField('name', validators=[DataRequired()])
    email=StringField('email', validators=[DataRequired()])
    email_password = StringField('email_password')
    birth_year = IntegerField('birth_year', validators=[DataRequired()])
    gender = StringField('gender', validators=[DataRequired()])
    grade_year = IntegerField('grade_year', validators=[DataRequired()])
    grade_state = StringField('grade_state', validators=[DataRequired()])
    occupation = StringField('occupation')
    work_state = StringField('work_state')
    app_ad = StringField('app_ad')

    last_update_date = DateField()



    