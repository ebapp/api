from api import mongo
import pymongo
from bson.objectid import ObjectId

collection = mongo.db.slideshow_col

def find(text_id):
    slides = list(collection.find({'text_id': ObjectId(text_id)}).sort('order_no', pymongo.ASCENDING))
    return slides

def insert(obj):
    collection.insert_one(obj)

def find_one(slide_id):
    slide = collection.find_one({'_id': ObjectId(slide_id)})
    return slide

def update(slide_id, obj):
    collection.find_one_and_update({'_id': ObjectId(slide_id)}, {'$set': obj})

def delete(slide_id):
    collection.delete_one({'_id': ObjectId(slide_id)})

def count(text_id):
    slides_count = collection.count_documents({'text_id': ObjectId(text_id)})
    return slides_count