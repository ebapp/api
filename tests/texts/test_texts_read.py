from flask import url_for
from bson.objectid import ObjectId


def test_login_required(client, test_text):
    response = client.get(url_for('routes.texts_read', text_id=str(test_text['_id'])), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_text, test_logged_user):
    response = client.get(url_for('routes.texts_read', text_id=str(test_text['_id'])), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_text, test_logged_user):
    response = client.post(url_for('routes.texts_read', text_id=str(test_text['_id'])), follow_redirects=True)
    assert response.status_code == 405


def test_success_read_data(client, test_text, test_logged_user, collection):
    response = client.get(url_for('routes.texts_read', text_id=str(test_text['_id'])), follow_redirects=True)
    assert str.encode(test_text['title']) in response.data
    assert str.encode(test_text['url_image']) in response.data
    assert str.encode(test_text['abstract']) in response.data
    assert str.encode(test_text['paper_link']) in response.data
    assert str.encode(test_text['last_update_date']) in response.data
    assert str.encode('Melhor evidência possível') in response.data

    for methodology_id in test_text['methodologies']:
        methodology_obj = collection.methodologies_col.find_one({'_id': ObjectId(methodology_id)})
        assert str.encode(methodology_obj['name']) in response.data

    for theme_id in test_text['themes']:
        theme_obj = collection.themes_col.find_one({'_id': ObjectId(theme_id)})
        assert str.encode(theme_obj['name']) in response.data

    for user_id in test_text['abstract_authors']:
        user_obj = collection.users_col.find_one({'_id': ObjectId(user_id)})
        assert str.encode(user_obj['name']) in response.data
