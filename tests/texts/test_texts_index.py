from flask import url_for


def test_login_required(client):
    response = client.get(url_for('routes.texts_index'), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_logged_user):
    response = client.get(url_for('routes.texts_index'), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_logged_user):
    response = client.post(url_for('routes.texts_index'), follow_redirects=True)
    assert response.status_code == 405


def test_success_list_data(client, test_text, test_logged_user):
    response = client.get(url_for('routes.texts_index'), follow_redirects=True)
    assert str.encode(test_text['title']) in response.data
    assert str.encode(test_text['last_update_date']) in response.data
