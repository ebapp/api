from flask import url_for, current_app as app
import pytest
import json

@pytest.fixture
def data():
    return dict(facebook_id='10',
                google_id = '15',
                name='Ze do raio',
                email='zedoraio@relampago.com',
                email_password = '4455667788',
                birth_year = 1910,
                gender = 'M',
                grade_year = 1967,
                grade_state = 'GO'
                )

def test_get_existing_facebook_id(client, app, collection, data):
    collection.dentists_col.insert_one(data)
    response = client.post(url_for('routes.get_dentist'),
                           data=json.dumps({'key': app.config['SECRET_KEY'], 'facebook_id': data['facebook_id']}),
                           content_type='application/json')
    collection.dentists_col.delete_one({'_id': data['_id']})
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['name'] == 'Ze do raio'

def test_get_existing_google_id(client, app, collection, data):
    collection.dentists_col.insert_one(data)
    response = client.post(url_for('routes.get_dentist'),
                           data=json.dumps({'key': app.config['SECRET_KEY'], 'google_id': data['google_id']}),
                           content_type = 'application/json')
    collection.dentists_col.delete_one({'_id': data['_id']})
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['name'] == 'Ze do raio'

def test_get_not_existing_facebook_id(client, app, collection, data):
    response = client.post(url_for('routes.get_dentist'),
                           data=json.dumps({'key': app.config['SECRET_KEY'], 'facebook_id': data['facebook_id']}),
                           content_type = 'application/json')
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['error'] == 'not found'

def test_get_not_existing_google_id(client, app, collection, data):
    response = client.post(url_for('routes.get_dentist'),
                           data=json.dumps({'key': app.config['SECRET_KEY'], 'google_id': data['google_id']}),
                           content_type = 'application/json')
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['error'] == 'not found'