from flask import url_for, json

headers = {
    'Content-Type': 'application/json'
}


def test_valid_key_data(client, test_user, test_admin_user, app):
    valid_key_data = dict(key=app.config['SECRET_KEY'], users_list=[str(test_user['_id']), str(test_admin_user['_id'])])
    response = client.post(url_for('routes.users_selected'), data=json.dumps(valid_key_data), headers=headers)
    returned_obj = json.loads(response.data)
    assert response.status_code == 200
    assert returned_obj['status'] == 200


def test_invalid_key_data(client, test_user, test_admin_user, app):
    invalid_key_data = dict(key=app.config['SECRET_KEY'] + 'x', users_list=[str(test_user['_id']), str(test_admin_user['_id'])])
    response = client.post(url_for('routes.users_selected'), data=json.dumps(invalid_key_data), headers=headers)
    returned_obj = json.loads(response.data)
    assert response.status_code == 200
    assert returned_obj['status'] == 401


def test_return_objects(client, test_user, test_admin_user, app):
    valid_key_data = dict(key=app.config['SECRET_KEY'], users_list=[str(test_user['_id']), str(test_admin_user['_id'])])
    response = client.post(url_for('routes.users_selected'), data=json.dumps(valid_key_data), headers=headers)
    returned_obj = json.loads(response.data)
    assert returned_obj['users'] is not None



