from flask import url_for
import pytest


@pytest.fixture
def data(test_user, test_registered_user):
    return dict(name='test_methodology',
                text='test_methodology_text',
                authors=[str(test_user['_id']), str(test_registered_user['_id'])],
                thermometer=5)


def test_login_required(client):
    response = client.get(url_for('routes.methodologies_create'), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_logged_user):
    response = client.get(url_for('routes.methodologies_create'), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_logged_user, collection, data):
    response = client.post(url_for('routes.methodologies_create'), data=data, follow_redirects=True)
    assert response.status_code == 200
    collection.methodologies_col.delete_many({'name': data['name']})


def test_valid_methodology_data_redirection(client, test_logged_user, collection, data):
    response = client.post(url_for('routes.methodologies_create'), data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'success' in response.data
    collection.methodologies_col.delete_many({'name': data['name']})


def test_invalid_methodology_data_redirection(client, test_logged_user, data):
    data['name'] = ''
    response = client.post(url_for('routes.methodologies_create'), data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'success' not in response.data


def test_create_object(client, test_logged_user, collection, data):
    client.post(url_for('routes.methodologies_create'), data=data, follow_redirects=True)
    methodology_obj = collection.methodologies_col.find_one({'name': data['name']})
    assert methodology_obj['name'] == data['name']
    assert methodology_obj['text'] == data['text']
    assert methodology_obj['authors'] == data['authors']
    assert methodology_obj['thermometer'] == data['thermometer']
    collection.methodologies_col.delete_many({'name': data['name']})
