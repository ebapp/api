from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import TextAreaField, IntegerField
from wtforms.validators import DataRequired


class SlidesForm(FlaskForm):
    description = TextAreaField('Descrição', validators=[DataRequired()])
    url_image = FileField('Imagem')
    order_no = IntegerField('Número do slide (ordem)', validators=[DataRequired()])

