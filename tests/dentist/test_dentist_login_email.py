from flask import url_for, current_app as app
import pytest
import json

@pytest.fixture
def data():
    return dict(facebook_id='',
                google_id = '',
                name='Ze do raio',
                email='zedoraio@relampago.com',
                email_password = "$2b$12$QM6dps2Es0LtNBRmRwX2RO1oQgpigl9tPMHM4Hkvw9NAvx0.aDk9y",
                birth_year = 1910,
                gender = 'M',
                grade_year = 1967,
                grade_state = 'GO'
                )

def test_dentist_login_email(client, app, collection, data):
    collection.dentists_col.insert_one(data)
    response = client.post(url_for('routes.dentists_login_email'), content_type='application/json',
                           data=json.dumps({'key': app.config['SECRET_KEY'], "email": data['email'],"email_password":"4455667788"}))
    collection.dentists_col.delete_one({'_id': data['_id']})
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['email'] == 'zedoraio@relampago.com'

def test_dentist_not_login_email(client, app, collection, data):
    response = client.post(url_for('routes.dentists_login_email'), content_type='application/json',
                           data=json.dumps({'key': app.config['SECRET_KEY'], "email": data['email'],"email_password":"4455667788"}))
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['error'] == 'Usuário não encontrado'

def test_dentist_login_email_wrong_password(client, app, collection, data):
    collection.dentists_col.insert_one(data)
    response = client.post(url_for('routes.dentists_login_email'), content_type='application/json',
                           data=json.dumps({'key': app.config['SECRET_KEY'], "email": data['email'],"email_password":"batata"}))
    collection.dentists_col.delete_one({'_id': data['_id']})
    assert response.is_json
    resp_obj = response.get_json()
    assert resp_obj['error'] == 'Senha inválida'

