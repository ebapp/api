$(document).ready(function(e) {
    var step = $(".step");
    var anchor = $(".anchor");
    var severity = $(".severity");
    var temperature;
    var thermometer = document.getElementById("thermometer");

    step.bind('click', function(ev) {
        var target = $(ev.target);

        step.each(function(ind, elem) {       
            var s = $(elem);
            s.removeClass().addClass("step");
            s.toggleClass(s.data("attr"), true);
            if(s.position().left <= target.position().left) {
                s.addClass(target.data("attr"));

                if(target.data("attr") == "info"){
                    thermometer.value = 1;
                    temperature = "Muito baixa";
                }
                if(target.data("attr") == "warning"){
                    thermometer.value = 2;
                    temperature = "Baixa";
                }
                if(target.data("attr") == "minor"){
                    thermometer.value = 3;
                    temperature = "Média";
                }
                if(target.data("attr") == "major"){
                    thermometer.value = 4;
                    temperature = "Alta";
                }
                if(target.data("attr") == "critical"){
                    thermometer.value = 5;
                    temperature = "Muito alta";
                }
            }
        });
      
    anchor.show().animate({
        left: (target.position().left + (target.width()/2) - 1) + "px"
    }, 300);
    
    severity.text(temperature);
    });
  });
