from flask import request, flash, redirect, render_template, url_for, current_app as app
from bson import json_util
import json
from api.routes import bp
from api.dbs import themes
from api import handlers
from api.models.themes import ThemesForm
from api.routes.user_routes import login_required
from datetime import datetime
from flask_cors import cross_origin


@bp.route("/themes/list", methods=['POST'])
@cross_origin()
def list_themes():
    response = {}

    content = request.json
    if content['key'] != app.config['SECRET_KEY']:
        handlers.errorUnauthorized('themes', response)
    else:
        themes_list = themes.find_all()
        if not handlers.successObjectReturned('themes', themes_list, response):
            handlers.successNoContent('themes', response)

    json_response = json.dumps(response, default=json_util.default, ensure_ascii=False).encode('utf8')

    return json_response


@bp.route('/themes/index', methods=['GET'])
@login_required
def themes_index():
    all_themes = themes.find_all()
    return render_template('themes/index.html', themes=all_themes)


@bp.route('/themes/create', methods=['GET', 'POST'])
@login_required
def themes_create():
    form = ThemesForm()
    if form.validate_on_submit():
        obj = {
            'name': request.form['name'],
            'color': request.form['color'],
            'last_update_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        }
        themes.insert(obj)
        flash('Tema criado com sucesso', 'success')
        return redirect(url_for('routes.themes_index'))
    return render_template('themes/form.html', title_label='Novo Tema', form=form, button_label='Criar')


@bp.route('/themes/read/<theme_id>', methods=['GET'])
@login_required
def themes_read(theme_id):
    theme = themes.find_one(theme_id)
    return render_template('themes/read.html', theme=theme)


@bp.route('/themes/update/<theme_id>', methods=['GET', 'POST'])
@login_required
def themes_update(theme_id):
    theme = themes.find_one(theme_id)
    form = ThemesForm(data=theme)
    if form.validate_on_submit():
        obj = {
            'name': request.form['name'],
            'color': request.form['color'],
            'last_update_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        }
        themes.update(theme_id, obj)
        flash('Tema atualizado com sucesso', 'success')
        return redirect(url_for('routes.themes_index'))
    return render_template('themes/form.html', title_label='Atualizar Tema', form=form, button_label='Atualizar')


@bp.route('/themes/delete/<theme_id>', methods=['POST'])
@login_required
def themes_delete(theme_id):
    themes.delete(theme_id)
    flash('Tema apagado com sucesso', 'success')
    return redirect(url_for('routes.themes_index'))
