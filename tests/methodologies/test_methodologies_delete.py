from flask import url_for


def test_login_required(client, test_methodology):
    response = client.post(url_for('routes.methodologies_delete', methodology_id=str(test_methodology['_id'])),
                           follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_methodology, test_logged_user, collection):
    response = client.get(url_for('routes.methodologies_delete', methodology_id=str(test_methodology['_id'])),
                          follow_redirects=True)
    assert response.status_code == 405


def test_post_response(client, test_methodology, test_logged_user, collection):
    response = client.post(url_for('routes.methodologies_delete', methodology_id=str(test_methodology['_id'])),
                           follow_redirects=True)
    assert response.status_code == 200


def test_success_delete_data(client, test_methodology, test_logged_user, collection):
    response = client.post(url_for('routes.methodologies_delete', methodology_id=str(test_methodology['_id'])),
                           follow_redirects=True)
    methodology_obj = collection.methodologies_col.find_one(test_methodology)
    assert b'success' in response.data
    assert not methodology_obj
