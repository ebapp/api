from flask import url_for

valid_user_obj = {
    'name': 'test_user',
    'email': 'new@test.com',
    'admin': False
}

invalid_user_obj = {
    'name': 'test_user',
    'email': 'test@test.com',
    'admin': False
}


def test_login_required(client, collection, test_user):
    client.post(url_for('routes.users_new'), data=valid_user_obj, follow_redirects=True)
    assert not collection.users_col.find_one({'email': 'new@test.com'})


def test_get_response(client, test_admin_user):
    response = client.get(url_for('routes.users_create', user_id=test_admin_user['_id']), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, collection, test_user, test_admin_user):
    response = client.post(url_for('routes.users_new'), data=valid_user_obj, follow_redirects=True)
    assert response.status_code == 200
    assert collection.users_col.delete_one(valid_user_obj)


def test_valid_user_obj_redirection(client, collection, test_user, test_admin_user):
    response = client.post(url_for('routes.users_new'), data=valid_user_obj, follow_redirects=True)
    assert b'success' in response.data
    assert collection.users_col.delete_one(valid_user_obj)


def test_invalid_user_obj_redirection(client, collection, test_user, test_admin_user):
    response = client.post(url_for('routes.users_new'), data=invalid_user_obj, follow_redirects=True)
    assert b'warning' in response.data
    assert collection.users_col.delete_one(valid_user_obj)


def test_create_object(client, collection, test_user, test_admin_user):
    client.post(url_for('routes.users_new'), data=valid_user_obj, follow_redirects=True)
    assert collection.users_col.find_one({'email': 'new@test.com'})
    assert collection.users_col.delete_one(valid_user_obj)
