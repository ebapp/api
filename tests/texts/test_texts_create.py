from flask import url_for
from io import BytesIO
import pytest


@pytest.fixture
def data(test_methodology, test_theme, test_user, test_registered_user):
    return dict(title='test_title',
                url_image=(BytesIO(b'test_data'), 'test_data.jpg'),
                abstract='test_abstract',
                methodologies=[str(test_methodology['_id'])],
                themes=[str(test_theme['_id'])],
                paper_link='http://www.test.com',
                abstract_authors=[str(test_user['_id']), str(test_registered_user['_id'])],
                study_relevance='1')


def test_login_required(client):
    response = client.get(url_for('routes.texts_create'), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_logged_user):
    response = client.get(url_for('routes.texts_create'), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_logged_user, collection, data):
    response = client.post(url_for('routes.texts_create'), data=data, follow_redirects=True)
    assert response.status_code == 200
    collection.texts_col.delete_many({'title': data['title']})


def test_valid_text_data_redirection(client, test_logged_user, collection, data):
    response = client.post(url_for('routes.texts_create'), data=data, content_type='multipart/form-data',
                           follow_redirects=True)
    assert b'success' in response.data
    collection.texts_col.delete_many({'title': data['title']})


def test_invalid_text_data_redirection(client, test_logged_user, data):
    data['title'] = ''
    response = client.post(url_for('routes.texts_create'), data=data, content_type='multipart/form-data',
                           follow_redirects=True)
    assert b'success' not in response.data

def test_invalid_text_without_title(client, test_logged_user, data):
    data["title"] = ''
    response = client.post(url_for('routes.texts_create'), data=data, content_type='multipart/form-data',
                           follow_redirects=True)

    assert b'Adicione um titulo' in response.data

def test_invalid_text_without_url_image(client, test_logged_user, data):
    data["url_image"] = ''
    response = client.post(url_for('routes.texts_create'), data=data, content_type='multipart/form-data',
                           follow_redirects=True)

    assert b'Adicione uma imagem' in response.data

def test_invalid_text_without_themes(client, test_logged_user, data):
    data["themes"] = ''
    response = client.post(url_for('routes.texts_create'), data=data, content_type='multipart/form-data',
                           follow_redirects=True)

    assert b'Adicione um tema' in response.data

def test_invalid_text_without_methodologies(client, test_logged_user, data):
    data["methodologies"] = ''
    response = client.post(url_for('routes.texts_create'), data=data, content_type='multipart/form-data',
                           follow_redirects=True)

    assert b'Adicione uma metodologia' in response.data

def test_invalid_text_without_paper_link(client, test_logged_user, data):
    data["paper_link"] = ''
    response = client.post(url_for('routes.texts_create'), data=data, content_type='multipart/form-data',
                           follow_redirects=True)

    assert b'Adicione uma url para o artigo' in response.data

def test_invalid_text_without_abstract_authors(client, test_logged_user, data):
    data["abstract_authors"] = ''
    response = client.post(url_for('routes.texts_create'), data=data, content_type='multipart/form-data',
                           follow_redirects=True)

    assert b'Adicione um autor' in response.data

def test_invalid_text_without_study_relevance(client, test_logged_user, data):
    data["study_relevance"] = ''
    response = client.post(url_for('routes.texts_create'), data=data, content_type='multipart/form-data',
                           follow_redirects=True)

    assert b'Adicione uma evidencia' in response.data

def test_create_object(client, test_logged_user, collection, data):
    client.post(url_for('routes.texts_create'), data=data, content_type='multipart/form-data', follow_redirects=True)
    text_obj = collection.texts_col.find_one({'title': data['title']})
    assert text_obj['title'] == data['title']
    assert text_obj['url_image']
    assert text_obj['abstract'] == data['abstract']
    assert text_obj['methodologies'] == data['methodologies']
    assert text_obj['themes'] == data['themes']
    assert text_obj['paper_link'] == data['paper_link']
    assert text_obj['abstract_authors'] == data['abstract_authors']
    assert text_obj['study_relevance'] == data['study_relevance']
    assert text_obj['posting_date']
    assert text_obj['last_update_date']
    collection.texts_col.delete_many({'title': data['title']})
