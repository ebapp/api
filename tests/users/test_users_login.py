from flask import url_for

valid_input = {
    'email': 'test_registered_user@test.com',
    'password': 'test_password'
}

invalid_input = {
    'email': 'test_invalid_user@test.com',
    'password': 'test_invalid_password'
}


def test_get_response(client):
    response = client.get(url_for('routes.users_login'))
    assert response.status_code == 200


def test_valid_input(client, test_registered_user):
    response = client.post(url_for('routes.users_login'), data=valid_input, follow_redirects=True)
    assert response.status_code == 200
    assert b'success' in response.data


def test_invalid_email(client, test_registered_user):
    response = client.post(url_for('routes.users_login'), data=invalid_input, follow_redirects=True)
    assert b'success' not in response.data


def test_invalid_password(client, test_registered_user):
    response = client.post(url_for('routes.users_login'), data=invalid_input, follow_redirects=True)
    assert b'success' not in response.data


def test_session_data(client, test_registered_user):
    client.post(url_for('routes.users_login'), data=valid_input, follow_redirects=True)
    with client.session_transaction() as sess:
        assert sess['user']
