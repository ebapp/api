from flask import Blueprint

bp = Blueprint('routes', __name__)

from api.routes import methodology_routes, text_routes, theme_routes, index_routes, user_routes, slideshow_routes, dentist_routes, privacy
