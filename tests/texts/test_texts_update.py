from flask import url_for
from io import BytesIO
import pytest


@pytest.fixture
def data(test_methodology, test_theme, test_user, test_registered_user):
    return dict(title='test_title_edited',
                url_image=(BytesIO(b'test_data_edited'), 'test_data_edited.jpg'),
                abstract='test_abstract_edited',
                methodologies=[str(test_methodology['_id']), str(test_methodology['_id'])],
                themes=[str(test_theme['_id']), str(test_theme['_id'])],
                paper_link='http://www.testedited.com',
                abstract_authors=[str(test_user['_id']), str(test_registered_user['_id'])],
                study_relevance='2')


def test_login_required(client, test_text):
    response = client.get(url_for('routes.texts_update', text_id=str(test_text['_id'])), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_text, test_logged_user):
    response = client.get(url_for('routes.texts_update', text_id=str(test_text['_id'])), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_text, test_logged_user, data):
    response = client.post(url_for('routes.texts_update', text_id=str(test_text['_id'])), data=data,
                           follow_redirects=True)
    assert response.status_code == 200


def test_success_update_data(client, test_text, test_logged_user, collection, data):
    response = client.post(url_for('routes.texts_update', text_id=str(test_text['_id'])), data=data,
                           follow_redirects=True)
    text_obj = collection.texts_col.find_one({'_id': test_text['_id']})
    assert b'success' in response.data
    assert text_obj['title'] == data['title']
    assert text_obj['url_image'] == url_for('routes.storage', filename=data['title'] + data['url_image'][1],
                                            _external=True)
    assert text_obj['abstract'] == data['abstract']
    assert text_obj['methodologies'] == data['methodologies']
    assert text_obj['themes'] == data['themes']
    assert text_obj['paper_link'] == data['paper_link']
    assert text_obj['abstract_authors'] == data['abstract_authors']
    assert text_obj['study_relevance'] == data['study_relevance']
    assert text_obj['posting_date'] == test_text['posting_date']
    assert not text_obj['last_update_date'] == test_text['last_update_date']


def test_fail_update_data(client, test_text, test_logged_user, collection, data):
    data['title'] = ''
    response = client.post(url_for('routes.texts_update', text_id=str(test_text['_id'])), data=data,
                           follow_redirects=True)
    text_obj = collection.texts_col.find_one({'_id': test_text['_id']})
    assert b'success' not in response.data
    assert not text_obj['title'] == data['title']
    assert not text_obj['url_image'] == url_for('routes.storage', filename=data['title'] + data['url_image'][1],
                                                _external=True)
    assert not text_obj['abstract'] == data['abstract']
    assert not text_obj['methodologies'] == data['methodologies']
    assert not text_obj['themes'] == data['themes']
    assert not text_obj['paper_link'] == data['paper_link']
    assert not text_obj['abstract_authors'] == data['abstract_authors']
    assert not text_obj['study_relevance'] == data['study_relevance']
    assert text_obj['posting_date'] == test_text['posting_date']
    assert text_obj['last_update_date'] == test_text['last_update_date']
