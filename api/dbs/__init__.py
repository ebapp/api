from flask import Blueprint

bp = Blueprint('dbs', __name__)

from api.dbs import themes, methodology, text, users, slideshow
