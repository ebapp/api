from flask_wtf import FlaskForm
from wtforms import StringField, RadioField, DateField
from wtforms.validators import DataRequired


class ThemesForm(FlaskForm):
    choices = [('#57A319', 'Kiwi'), ('#5CD444', 'Limão'), ('#ADE496', 'Goiaba'), ('#B84335', 'Maçã'),
               ('#F67054', 'Melancia'), ('#E4844C', 'Laranja'), ('#D7AD1E', 'Melão'), ('#F1D829', 'Abacaxi'),
               ('#F4E484', 'Carambola'), ('#67729C', 'Jaboticaba'), ('#9CB7DE', 'Mirtilo'), ('#B2C3E4', 'Uva')]

    name = StringField('Nome do tema', validators=[DataRequired()])
    color = RadioField('Cor do tema', choices=choices, validators=[DataRequired()])
    last_update_date = DateField()
