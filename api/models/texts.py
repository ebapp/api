from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, RadioField, DateField, TextAreaField, SelectMultipleField
from wtforms.validators import DataRequired, url
from wtforms.fields.html5 import URLField


class TextsForm(FlaskForm):
    choices = [('1', 'Melhor evidência disponível e possível'), ('2', 'Melhor evidência disponível mas não possível'),
               ('3', 'Existem evidências de maior nível disponíveis')]

    title = StringField('Título', validators=[DataRequired()])
    url_image = FileField('Imagem')
    abstract_intro = TextAreaField('Resumo antes do slideshow')
    abstract = TextAreaField('Resumo após o slideshow')
    methodologies = SelectMultipleField('Metodologias', validators=[DataRequired()])
    themes = SelectMultipleField('Temas', validators=[DataRequired()])
    paper_link = URLField('URL para o artigo', validators=[url()])
    abstract_authors = SelectMultipleField('Autores', validators=[DataRequired()])
    study_relevance = RadioField('Nível de Evidência', choices=choices, validators=[DataRequired()])
    posting_date = DateField()
    last_update_date = DateField()
