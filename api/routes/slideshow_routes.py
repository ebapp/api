from flask import redirect, render_template, url_for, request, flash, send_from_directory, current_app as app
from api.dbs import slideshow
from datetime import datetime
from api.routes import bp
import os
from api.routes.user_routes import login_required
from api.models.slides import SlidesForm
from werkzeug.utils import secure_filename
from api.dbs import slideshow
from bson.objectid import ObjectId
from flask_wtf.file import FileRequired
from flask_cors import cross_origin
from api import handlers
from bson import json_util
import json

@bp.route('/slideshows/index/<text_id>', methods=['GET'])
@login_required
def slideshow_index(text_id):
    slides = slideshow.find(text_id)
    return render_template('slideshows/index.html', slides=slides, text_id = text_id)

@bp.route('/slides/create/<text_id>', methods=['GET', 'POST'])
@login_required
def slide_create(text_id):
    form = SlidesForm()
    form.url_image.validators.append(FileRequired())
    if request.method == 'GET':
        form.order_no.data = slideshow.count(text_id) + 1
        
    if form.validate_on_submit():
        f = form.url_image.data
        filename = secure_filename(text_id + "-" + datetime.now().strftime("%Y%m%d%H%M%S") + "-" + f.filename)
        f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        obj = {
            'url_image': url_for('routes.storage', filename=filename, _external=True),
            'text_id': ObjectId(text_id),
            'description': form.description.data,
            'order_no': form.order_no.data,
            'posting_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'last_update_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        }
        slideshow.insert(obj)
        flash('Slide criado com sucesso', 'success')
        return redirect(url_for('routes.slideshow_index', text_id=text_id))

    if 'url_image' in form.errors:
        flash('Adicione uma imagem', 'danger')
    if 'description' in form.errors:
        flash('Adicione uma descrição', 'danger')
    if 'order_no' in form.errors:
        flash('Adicione um numero de ordem', 'danger')

    return render_template('slideshows/form.html', title_label='Criar Slide', form=form, button_label='Criar')

@bp.route('/slides/read/<slide_id>', methods=['GET'])
@login_required
def slide_read(slide_id):
    slide_obj = slideshow.find_one(slide_id)
    return render_template('slideshows/read.html', slide=slide_obj)

@bp.route('/slides/update/<slide_id>', methods=['GET', 'POST'])
@login_required
def slide_update(slide_id):
    slide_obj = slideshow.find_one(slide_id)
    form = SlidesForm(data=slide_obj )

    if form.validate_on_submit():
        if 'url_image' in request.files:
            f = form.url_image.data
            filename = secure_filename(slide_id + "-" + datetime.now().strftime("%Y%m%d%H%M%S") + "-" + f.filename)
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            slide_obj['url_image'] = url_for('routes.storage', filename=filename, _external=True)
        slide_obj['description'] = form.description.data
        slide_obj['order_no'] = form.order_no.data
        slideshow.update(slide_id, slide_obj)
        flash('Slide atualizado com sucesso', 'success')
        return redirect(url_for('routes.slideshow_index', text_id=slide_obj['text_id']))
    return render_template('slideshows/form.html', title_label='Atualizar slide', form=form, button_label='Atualizar')

@bp.route('/slides/delete/<slide_id>', methods=['POST'])
@login_required
def slide_delete(slide_id):
    slide_obj = slideshow.find_one(slide_id)
    slideshow.delete(slide_id)
    flash('Slide removido com sucesso', 'success')
    return redirect(url_for('routes.slideshow_index', text_id=slide_obj['text_id']))

@bp.route('/slideshow', methods=['GET'])
@cross_origin()
def slideshow_get():
    if request.method == 'GET':
        response = {}
        slides = slideshow.find(request.args['text_id'])
        if not handlers.successObjectReturned('slides', slides, response):
            handlers.successNoContent('slides', response)

        json_response = json.dumps(response, default=json_util.default, ensure_ascii=False).encode('utf8')
        return json_response
