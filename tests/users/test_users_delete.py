from flask import url_for
from bson.objectid import ObjectId


def test_login_required(client, collection, test_user):
    response = client.post(url_for('routes.users_delete', user_id=test_user['_id']), follow_redirects=True)
    deleted_user = collection.users_col.find_one({'_id': ObjectId(test_user['_id'])})
    assert deleted_user
    assert b'danger' in response.data


def test_get_response(client, test_user, test_admin_user):
    response = client.post(url_for('routes.users_delete', user_id=test_user['_id']), follow_redirects=True)
    assert response.status_code == 200


def test_success_delete_data(client, test_user, test_admin_user, collection):
    client.post(url_for('routes.users_delete', user_id=test_user['_id']), follow_redirects=True)
    deleted_user = collection.users_col.find_one({'_id': ObjectId(test_user['_id'])})
    assert deleted_user is None
