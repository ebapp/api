from flask import url_for


def test_login_required(client, slide_test):
    response = client.post(url_for('routes.slide_delete', slide_id=str(slide_test['_id'])),
                           follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, slide_test, test_logged_user, collection):
    response = client.get(url_for('routes.slide_delete', slide_id=str(slide_test['_id'])),
                          follow_redirects=True)
    assert response.status_code == 405


def test_post_response(client, slide_test, test_logged_user, collection):
    response = client.post(url_for('routes.slide_delete', slide_id=str(slide_test['_id'])),
                           follow_redirects=True)
    assert response.status_code == 200


def test_success_delete_data(client, slide_test, test_logged_user, collection):
    response = client.post(url_for('routes.slide_delete', slide_id=str(slide_test['_id'])),
                           follow_redirects=True)
    slide = collection.slideshow_col.find_one(slide_test)
    assert b'success' in response.data
    assert not slide
