from flask import url_for
from werkzeug.security import check_password_hash
import pytest


@pytest.fixture
def data():
    return dict(name='test_user',
                email='test@test.com',
                password='test_password',
                confirm='test_password',
                about='about_test_user')


def test_get_response(client, test_user):
    response = client.get(url_for('routes.users_create', user_id=test_user['_id']), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, collection, test_user, data):
    response = client.post(url_for('routes.users_create', user_id=test_user['_id']), data=data,
                           follow_redirects=True)
    assert response.status_code == 200


def test_valid_user_data_redirection(client, collection, test_user, data):
    response = client.post(url_for('routes.users_create', user_id=test_user['_id']), data=data,
                           follow_redirects=True)
    assert b'success' in response.data


def test_invalid_user_data_redirection(client, collection, test_user, data):
    data['confirm'] = 'wrong_password'
    response = client.post(url_for('routes.users_create', user_id=test_user['_id']), data=data,
                           follow_redirects=True)
    assert b'success' not in response.data


def test_create_object(client, collection, test_user, data):
    response = client.post(url_for('routes.users_create', user_id=test_user['_id']), data=data, follow_redirects=True)
    user_obj = collection.users_col.find_one({'_id': test_user['_id']})
    assert b'success' in response.data
    assert user_obj['name'] == data['name']
    assert user_obj['email'] == data['email']
    assert check_password_hash(user_obj['password'], data['password'])
    assert user_obj['about'] == data['about']
