from flask import url_for
from bson.objectid import ObjectId


def test_login_required(client, test_methodology):
    response = client.get(url_for('routes.methodologies_read', methodology_id=str(test_methodology['_id'])),
                          follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_methodology, test_logged_user):
    response = client.get(url_for('routes.methodologies_read', methodology_id=str(test_methodology['_id'])),
                          follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_methodology, test_logged_user):
    response = client.post(url_for('routes.methodologies_read', methodology_id=str(test_methodology['_id'])),
                           follow_redirects=True)
    assert response.status_code == 405


def test_success_read_data(client, test_methodology, test_logged_user, collection):
    response = client.get(url_for('routes.methodologies_read', methodology_id=str(test_methodology['_id'])),
                          follow_redirects=True)
    assert str.encode(test_methodology['name']) in response.data
    assert str.encode(test_methodology['text']) in response.data
    assert str.encode('Muito alto') in response.data

    for user_id in test_methodology['authors']:
        user_obj = collection.users_col.find_one({'_id': ObjectId(user_id)})
        assert str.encode(user_obj['name']) in response.data
