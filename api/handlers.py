def successNoContent(objectName, response):
    response['status'] = 204
    response['message'] = "There are no " + objectName + " found."
    return


def successObjectReturned(objectName, item, response):
    if (item):
        response[objectName] = item
        response['status'] = 200
        response['message'] = "Success, " + objectName + " returned."
        return True
    return False


def errorUnauthorized(objectName, response):
    response[objectName] = []
    response['status'] = 401
    response['message'] = "You are unauthorized."
    return
