from flask import url_for


def test_login_required(client, test_theme):
    response = client.post(url_for('routes.themes_delete', theme_id=str(test_theme['_id'])), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_theme, test_logged_user):
    response = client.get(url_for('routes.themes_delete', theme_id=str(test_theme['_id'])), follow_redirects=True)
    assert response.status_code == 405


def test_post_response(client, test_theme, test_logged_user):
    response = client.post(url_for('routes.themes_delete', theme_id=str(test_theme['_id'])), follow_redirects=True)
    assert response.status_code == 200


def test_success_delete_data(client, test_theme, test_logged_user, collection):
    response = client.post(url_for('routes.themes_delete', theme_id=str(test_theme['_id'])), follow_redirects=True)
    theme_obj = collection.themes_col.find_one({'_id': test_theme['_id']})
    assert b'success' in response.data
    assert not theme_obj
