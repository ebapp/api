from werkzeug.security import generate_password_hash
from api.dbs import users

admin = dict(name='Administrador',
             email='admin@ebapp.com',
             password=generate_password_hash('admin', method='sha256'),
             about='Administrador temporário durante o período de desenvolvimento',
             admin=True)

users.insert(admin)
