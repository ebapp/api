from flask import url_for


def test_login_required(client, test_registered_user):
    response = client.get(url_for('routes.users_read', user_id=str(test_registered_user['_id'])), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_registered_user, test_admin_user):
    response = client.get(url_for('routes.users_read', user_id=str(test_registered_user['_id'])))
    assert response.status_code == 200


def test_post_response(client, test_registered_user, test_admin_user):
    response = client.post(url_for('routes.users_read', user_id=str(test_registered_user['_id'])))
    assert response.status_code == 405


def test_success_read_data(client, test_registered_user, test_admin_user):
    response = client.get(url_for('routes.users_read', user_id=str(test_registered_user['_id'])), follow_redirects=True)
    assert str.encode(test_registered_user['name']) in response.data
    assert str.encode(test_registered_user['email']) in response.data
    assert str.encode(test_registered_user['about']) in response.data
