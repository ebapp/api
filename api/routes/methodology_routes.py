from flask import request, flash, redirect, render_template, url_for, current_app as app
from bson import json_util
import json
from api.routes import bp
from api.dbs import methodology, users
from api.models.methodologies import MethodologiesForm
from api import handlers
from datetime import datetime
from flask_cors import cross_origin
from api.routes.user_routes import login_required


@bp.route("/methodology/detail", methods=['POST'])
@cross_origin()
def show_methodology():
    if request.method == 'POST':
        response = {}

        content = request.json

        if content['key'] != app.config['SECRET_KEY']:
            handlers.errorUnauthorized('methodologies', response)
        else:
            methodology_detail = methodology.find_one(content['_id'])
            if not handlers.successObjectReturned('methodologies', methodology_detail, response):
                handlers.successNoContent('methodologies', response)

        json_response = json.dumps(response, default=json_util.default, ensure_ascii=False).encode('utf8')

        return json_response


@bp.route("/methodologies/selected", methods=['POST'])
@cross_origin()
def methodologies_selected():
    if request.method == 'POST':
        response = {}

        content = request.json

        if content['key'] != app.config['SECRET_KEY']:
            handlers.errorUnauthorized('methodologies', response)
        else:
            selected_methodologies = list(methodology.find_selected(content['methodologies_list']))
            if not handlers.successObjectReturned('methodologies', selected_methodologies, response):
                handlers.successNoContent('methodologies', response)

        json_response = json.dumps(response, default=json_util.default, ensure_ascii=False).encode('utf8')

        return json_response


@bp.route('/methodologies/index', methods=['GET'])
@login_required
def methodologies_index():
    all_methodologies = methodology.find_all()
    return render_template('methodologies/index.html', methodologies=all_methodologies)


@bp.route('/methodologies/create', methods=['GET', 'POST'])
@login_required
def methodologies_create():
    all_authors = users.find_all()
    form = MethodologiesForm()
    form.authors.choices = [(str(author_obj['_id']), author_obj['name']) for author_obj in all_authors]

    if form.validate_on_submit():
        obj = {
            'name': form.name.data,
            'text': form.text.data,
            'authors': form.authors.data,
            'thermometer': form.thermometer.data,
            'last_update_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        }
        methodology.insert(obj)
        flash('Metodologia adicionada com sucesso', 'success')
        return redirect(url_for('routes.methodologies_index'))
    return render_template('methodologies/form.html', title_label='Criar Metodologia', form=form, button_label='Criar')


@bp.route('/methodologies/read/<methodology_id>', methods=['GET'])
@login_required
def methodologies_read(methodology_id):
    methodology_obj = methodology.find_one(methodology_id)
    authors = users.find_selected(methodology_obj['authors'])
    return render_template('methodologies/read.html', methodology=methodology_obj, authors=authors)


@bp.route('/methodologies/update/<methodology_id>', methods=['GET', 'POST'])
@login_required
def methodologies_update(methodology_id):
    methodology_obj = methodology.find_one(methodology_id)
    all_authors = users.find_all()
    form = MethodologiesForm(data=methodology_obj)
    form.authors.choices = [(str(author_obj['_id']), author_obj['name']) for author_obj in all_authors]
    if form.validate_on_submit():
        obj = {
            'name': form.name.data,
            'text': form.text.data,
            'authors': form.authors.data,
            'thermometer': form.thermometer.data,
            'last_update_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        }
        methodology.update(methodology_id, obj)
        flash('Metodologia atualizada com sucesso', 'success')
        return redirect(url_for('routes.methodologies_index'))
    return render_template('methodologies/form.html', title_label='Atualizar Metodologia', form=form,
                           button_label='Atualizar')


@bp.route('/methodologies/delete/<methodology_id>', methods=['POST'])
@login_required
def methodologies_delete(methodology_id):
    methodology.delete(methodology_id)
    flash('Metodologia removida com sucesso', 'success')
    return redirect(url_for('routes.methodologies_index'))
