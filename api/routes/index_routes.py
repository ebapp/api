from flask import render_template, current_app as app
from api.routes import bp
from oauth2client.service_account import ServiceAccountCredentials
import os


# Defines a method to get an access token from the ServiceAccount object.
def get_access_token():
    # The scope for the OAuth2 request.
    SCOPE = 'https://www.googleapis.com/auth/analytics.readonly'

    # The location of the key file with the key data.
    KEY_FILEPATH = os.path.join(app.root_path, "static", "evident-224520-b078dc00f01c.json")
    return ServiceAccountCredentials.from_json_keyfile_name(KEY_FILEPATH, SCOPE).get_access_token().access_token


@bp.route('/', methods=['GET'])
def index():
    token = get_access_token()
    return render_template('index.html', token=token)
