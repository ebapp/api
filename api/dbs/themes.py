from api import mongo
import pymongo
from bson.objectid import ObjectId

collection = mongo.db.themes_col 


# insert theme to database
def insert(obj):
    collection.insert_one(obj)


# update theme from database
def update(theme_id, obj):
    collection.find_one_and_update({'_id': ObjectId(theme_id)}, {'$set': obj})


# delete theme from database
def delete(theme_id):
    collection.delete_one({'_id': ObjectId(theme_id)})


def find_all():
    return list(collection.find().sort("last_update_date", pymongo.DESCENDING))


def find(theme_id):
    return collection.find({'_id': ObjectId(theme_id)})


def find_one(theme_id):
    return collection.find_one({'_id': ObjectId(theme_id)})


def find_selected(themes_list):
    return collection.find({'_id': {'$in': [ObjectId(theme_id) for theme_id in themes_list]}})
