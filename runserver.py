from api import app

app.run(host='0.0.0.0', port=443, 
        ssl_context=('/etc/letsencrypt/live/evident.gq/fullchain.pem', 
                     '/etc/letsencrypt/live/evident.gq/privkey.pem'))
