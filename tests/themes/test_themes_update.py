from flask import url_for

data = dict(name='test_theme_edited',
            color='#5CD444')


def test_login_required(client, test_theme):
    response = client.get(url_for('routes.themes_update', theme_id=str(test_theme['_id'])), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_theme, test_logged_user):
    response = client.get(url_for('routes.themes_update', theme_id=str(test_theme['_id'])), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_theme, test_logged_user):
    response = client.post(url_for('routes.themes_update', theme_id=str(test_theme['_id'])), data=data,
                           follow_redirects=True)
    assert response.status_code == 200


def test_success_update_data(client, test_theme, test_logged_user, collection):
    response = client.post(url_for('routes.themes_update', theme_id=str(test_theme['_id'])), data=data,
                           follow_redirects=True)
    theme_obj = collection.themes_col.find_one({'_id': test_theme['_id']})
    assert b'success' in response.data
    assert theme_obj['name'] == data['name']
    assert theme_obj['color'] == data['color']
    assert not theme_obj['last_update_date'] == '0000-00-00 00:00:00'


def test_fail_update_data(client, test_theme, test_logged_user, collection):
    data['color'] = '#000000'
    response = client.post(url_for('routes.themes_update', theme_id=str(test_theme['_id'])), data=data,
                           follow_redirects=True)
    theme_obj = collection.themes_col.find_one({'_id': test_theme['_id']})
    assert b'success' not in response.data
    assert not theme_obj['name'] == data['name']
    assert not theme_obj['color'] == data['color']
    assert theme_obj['last_update_date'] == '0000-00-00 00:00:00'
