from api import mongo
from bson.objectid import ObjectId

collection = mongo.db.like_col


def update_one(text_id, dentist_id, state):
    collection.update_one(filter={'text_id': ObjectId(text_id), 'dentist_id': ObjectId(dentist_id)},
                          update={'$set': {'state': state}},
                          upsert=True)

def find_one(text_id, dentist_id):
    return collection.find_one({'text_id': ObjectId(text_id), 'dentist_id': ObjectId(dentist_id)})

def delete_one(text_id, dentist_id):
    collection.delete_one({'text_id': ObjectId(text_id), 'dentist_id': ObjectId(dentist_id)})

def count_likes(text_id):
    return collection.count_documents({'text_id': ObjectId(text_id), 'state': 'like'})

def count_dislikes(text_id):
    return collection.count_documents({'text_id': ObjectId(text_id), 'state': 'dislike'})
