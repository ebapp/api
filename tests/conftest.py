from api import create_app, mongo
from werkzeug.security import generate_password_hash
import pytest
from bson.objectid import ObjectId


@pytest.fixture
def app():
    app = create_app()
    app.config['TESTING'] = True
    app.config['WTF_CSRF_ENABLED'] = False
    ctx = app.test_request_context()
    ctx.push()
    yield app
    ctx.pop()


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def collection():
    return mongo.db


@pytest.fixture
def test_user(collection):
    test_obj = dict(name='test_user',
                    email='test@test.com',
                    admin=False)
    collection.users_col.insert_one(test_obj)
    yield collection.users_col.find_one({'email': 'test@test.com'})
    collection.users_col.delete_many({'name': {"$regex": 'test', "$options": "i"}})


@pytest.fixture
def test_registered_user(collection):
    test_obj = dict(name='test_registered_user',
                    email='test_registered_user@test.com',
                    password=generate_password_hash('test_password', method='sha256'),
                    about='about_test_registered_user',
                    admin=False)
    collection.users_col.insert_one(test_obj)
    yield collection.users_col.find_one({'email': 'test_registered_user@test.com'})
    collection.users_col.delete_many({'name': {"$regex": 'test', "$options": "i"}})


@pytest.fixture
def test_logged_user(client, test_registered_user):
    with client.session_transaction() as sess:
        sess['user'] = dict(id=str(test_registered_user['_id']),
                            name=test_registered_user['name'],
                            email=test_registered_user['email'],
                            admin=test_registered_user['admin'])
    yield test_registered_user
    with client.session_transaction() as sess:
        sess.clear()


@pytest.fixture
def test_admin_user(client, collection):
    test_obj = dict(name='test_admin_user',
                    email='test_admin_user@test.com',
                    password=generate_password_hash('test_admin_password', method='sha256'),
                    about='about_test_admin_user',
                    admin=True)
    collection.users_col.insert_one(test_obj)
    test_admin = collection.users_col.find_one({'email': 'test_admin_user@test.com'})
    with client.session_transaction() as sess:
        sess['user'] = dict(id=str(test_admin['_id']),
                            name=test_admin['name'],
                            email=test_admin['email'],
                            admin=test_admin['admin'])
    yield test_admin
    with client.session_transaction() as sess:
        sess.clear()
    collection.users_col.delete_many({'name': {"$regex": 'test', "$options": "i"}})


@pytest.fixture
def test_methodology(collection, test_user, test_registered_user):
    test_obj = dict(name='test_methodology',
                    text='test_methodology_text',
                    authors=[str(test_user['_id'])],
                    thermometer=5,
                    last_update_date='0000-00-00 00:00:00')
    collection.methodologies_col.insert_one(test_obj)
    yield collection.methodologies_col.find_one({'name': 'test_methodology'})
    collection.methodologies_col.delete_many({'name': {"$regex": 'test', "$options": "i"}})


@pytest.fixture
def test_theme(collection):
    test_obj = dict(name='test_theme',
                    color='#57A319',
                    last_update_date='0000-00-00 00:00:00')
    collection.themes_col.insert_one(test_obj)
    yield collection.themes_col.find_one({'name': 'test_theme'})
    collection.themes_col.delete_many({'name': {"$regex": 'test', "$options": "i"}})


@pytest.fixture
def test_text(collection, test_methodology, test_theme, test_user):
    test_obj = dict(title='test_title',
                    url_image='http://test_image.jpg',
                    abstract='test_abstract',
                    methodologies=[str(test_methodology['_id'])],
                    themes=[str(test_theme['_id'])],
                    paper_link='http://test_paper_link.com',
                    abstract_authors=[str(test_user['_id'])],
                    study_relevance='1',
                    posting_date='0000-00-00 00:00:00',
                    last_update_date='0000-00-00 00:00:00')
    collection.texts_col.insert_one(test_obj)
    yield collection.texts_col.find_one({'title': 'test_title'})
    collection.texts_col.delete_many({'title': {'$regex': 'test', '$options': 'i'}})

@pytest.fixture
def slide_test(collection, test_text):
    slide_obj = dict(title='test_title',
                    url_image='http://test_image.jpg',
                    description='Description slide',
                    text_id = ObjectId(test_text['_id']),
                    order_no='1',
                    posting_date='0000-00-00 00:00:00',
                    last_update_date='0000-00-00 00:00:00')
    collection.slideshow_col.insert_one(slide_obj)
    yield collection.slideshow_col.find_one({'_id':slide_obj['_id']})
    
    collection.slideshow_col.delete_one({'_id':slide_obj['_id']})

@pytest.fixture
def dentist_test(collection):
    dentist_obj = dict(facebook_id='10',
                google_id = '15',
                name='Ze do raio',
                email='zedoraio@relampago.com',
                email_password = '4455667788',
                birth_year = 1910,
                gender = 'M',
                grade_year = 1967,
                grade_state = 'GO')
    collection.dentist_col.insert_one(dentist_obj)
    yield collection.dentist_col.find_one({'_id':dentist_obj['_id']})
    collection.dentist_col.delete_one({'_id':dentist_obj['_id']})

@pytest.fixture
def like_test(collection, test_text, dentist_test):
    like_obj = dict(text_id = ObjectId(test_text['_id']),
                    dentist_id = ObjectId(dentist_test['_id']),
                    state = 'like')
    collection.like_col.insert_one(like_obj)
    yield collection.like_col.find_one({'_id': like_obj['_id']})
    collection.like_col.delete_one({'_id': like_obj['_id']})

@pytest.fixture
def dislike_test(collection, test_text, dentist_test):
    like_obj = dict(text_id = ObjectId(test_text['_id']),
                    dentist_id = ObjectId(dentist_test['_id']),
                    state = 'dislike')
    collection.like_col.insert_one(like_obj)
    yield collection.like_col.find_one({'_id': like_obj['_id']})
    collection.like_col.delete_one({'_id': like_obj['_id']})

another_dentist_test = dentist_test

@pytest.fixture
def another_like_test(collection, test_text, another_dentist_test):
    like_obj = dict(text_id = ObjectId(test_text['_id']),
                    dentist_id = ObjectId(another_dentist_test['_id']),
                    state = 'like')
    collection.like_col.insert_one(like_obj)
    yield collection.like_col.find_one({'_id': like_obj['_id']})
    collection.like_col.delete_one({'_id': like_obj['_id']})

@pytest.fixture
def another_dislike_test(collection, test_text, another_dentist_test):
    like_obj = dict(text_id = ObjectId(test_text['_id']),
                    dentist_id = ObjectId(another_dentist_test['_id']),
                    state = 'dislike')
    collection.like_col.insert_one(like_obj)
    yield collection.like_col.find_one({'_id': like_obj['_id']})
    collection.like_col.delete_one({'_id': like_obj['_id']})
