from flask import Flask, request, redirect

app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def hello(path):
   url = request.url.replace('http://', 'https://', 1)
   return redirect(url, code=301)

app.run(host='0.0.0.0', port=80)
