from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, BooleanField, PasswordField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, EqualTo


class RegisterForm(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired()])
    admin = BooleanField('Administrador', default=False)


class RegistrationForm(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired()], render_kw={'readonly': True})
    password = PasswordField('Senha', validators=[DataRequired()])
    confirm = PasswordField('Repetir Senha', validators=[EqualTo('password', message='Senhas diferentes')])
    about = TextAreaField('Sobre')
    admin = BooleanField('Administrador')


class LoginForm(FlaskForm):
    email = EmailField('Email', validators=[DataRequired()])
    password = PasswordField('Senha', validators=[DataRequired()])


class EmailForm(FlaskForm):
    email = EmailField('Email', validators=[DataRequired()])
