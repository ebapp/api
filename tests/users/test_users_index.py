from flask import url_for


def test_login_required(client, test_user):
    response = client.get(url_for('routes.users_index'), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_admin_user):
    response = client.get(url_for('routes.users_index'), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_admin_user):
    response = client.post(url_for('routes.users_index'), follow_redirects=True)
    assert response.status_code == 405


def test_success_list_data(client, test_user, test_admin_user):
    response = client.get(url_for('routes.users_index'), follow_redirects=True)
    assert str.encode(test_user['name']) in response.data
    assert str.encode(test_user['email']) in response.data
    assert str.encode(test_admin_user['name']) in response.data
    assert str.encode(test_admin_user['email']) in response.data
