from flask import render_template, redirect, url_for, flash, session, request, json, current_app as app
from api.routes import bp
from api.dbs import users
from api import handlers
from api.models.users import RegisterForm, RegistrationForm, LoginForm, EmailForm
from werkzeug.security import generate_password_hash, check_password_hash
from flask_cors import cross_origin
from bson import json_util
from functools import wraps


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('user') is None:
            flash('Usuário não autenticado.', 'danger')
            return redirect(url_for('routes.index'))
        return f(*args, **kwargs)
    return decorated_function


@bp.route('/users/selected', methods=['POST'])
@cross_origin()
def users_selected():
    if request.method == 'POST':
        response = {}

        content = request.json

        if content['key'] != app.config['SECRET_KEY']:
            handlers.errorUnauthorized('users', response)
        else:
            selected_users = list(users.find_selected(content['users_list']))
            if not handlers.successObjectReturned('users', selected_users, response):
                handlers.successNoContent('users', response)

        json_response = json.dumps(response, default=json_util.default, ensure_ascii=False).encode('utf-8')

        return json_response


@bp.route('/users/index', methods=['GET'])
@login_required
def users_index():
    all_users = users.find_all()
    return render_template('users/index.html', users=all_users)


@bp.route('/users/new', methods=['GET', 'POST'])
@login_required
def users_new():
    form = RegisterForm()
    if form.validate_on_submit():
        if not users.find_by_email(form.email.data):
            user_obj = {
                'name': form.name.data,
                'email': form.email.data,
                'admin': form.admin.data
            }
            users.insert(user_obj)
            flash('Usuário registrado com sucesso', 'success')
            return redirect(url_for('routes.users_index'))
        flash('Já existe um usuário com este Email', 'warning')
    return render_template('users/register.html', form=form)


@bp.route('/users/verify', methods=['GET', 'POST'])
def users_verify():
    form = EmailForm()
    if form.validate_on_submit():
        user_obj = users.find_by_email(form.email.data)
        if user_obj:
            if 'password' in user_obj:
                flash('Usuário já cadastrado. Faça o Login:', 'warning')
                return redirect(url_for('routes.users_login'))
            return redirect(url_for('routes.users_create', user_id=str(user_obj['_id'])))
        flash('Usuário sem permissão para cadastro', 'warning')
    return render_template('users/verify.html', title_label='Insira o Email', form=form, button_label='Verificar')


@bp.route('/users/create/<user_id>', methods=['GET', 'POST'])
def users_create(user_id):
    user_obj = users.find_one(user_id)
    form = RegistrationForm(data=user_obj)
    if form.validate_on_submit():
        user_obj['name'] = form.name.data
        user_obj['password'] = generate_password_hash(form.password.data, method='sha256')
        user_obj['about'] = form.about.data
        users.update(str(user_obj['_id']), user_obj)
        flash('Usuário cadastrado com sucesso', 'success')
        return redirect(url_for('routes.users_login'))
    return render_template('users/form.html', title_label='Cadastro', form=form, button_label='Cadastrar')


@bp.route('/users/read/<user_id>', methods=['GET'])
@login_required
def users_read(user_id):
    user_obj = users.find_one(user_id)
    return render_template('users/read.html', user=user_obj)


@bp.route('/users/update/<user_id>', methods=['GET', 'POST'])
@login_required
def users_update(user_id):
    user_obj = users.find_one(user_id)
    form = RegistrationForm(data=user_obj)
    if form.validate_on_submit():
        user_obj['name'] = form.name.data
        user_obj['password'] = generate_password_hash(form.password.data, method='sha256')
        user_obj['about'] = form.about.data
        users.update(user_id, user_obj)
        flash('Usuário atualizado com sucesso', 'success')
        return redirect(url_for('routes.index'))
    return render_template('users/form.html', title_label='Atualizar Cadastro', form=form, button_label='Atualizar')


@bp.route('/users/delete/<user_id>', methods=['POST'])
@login_required
def users_delete(user_id):
    users.delete(user_id)
    if user_id == session['user']['id']:
        return redirect(url_for('routes.users_logout'))
    return redirect(url_for('routes.users_index'))


@bp.route('/users/login', methods=['GET', 'POST'])
def users_login():
    form = LoginForm()
    if form.validate_on_submit():
        user_obj = users.find_by_email(form.email.data)
        if user_obj:
            if 'password' in user_obj:
                if check_password_hash(user_obj['password'], form.password.data):
                    login_data = {
                        'id': str(user_obj['_id']),
                        'name': user_obj['name'],
                        'email': user_obj['email'],
                        'admin': user_obj['admin']
                    }
                    session['user'] = login_data
                    flash('Bem-vindo, ' + user_obj['name'], 'success')
                    return redirect(url_for('routes.index'))
                else:
                    flash('Senha incorreta', 'warning')
            else:
                flash('Usuário sem cadastro finalizado. Faça o cadastro:', 'warning')
                return redirect(url_for('routes.users_verify'))
        else:
            flash('Usuário não cadastrado', 'warning')
    return render_template('users/login.html', form=form)


@bp.route('/users/logout', methods=['GET'])
@login_required
def users_logout():
    session.clear()
    flash('Usuário saiu com sucesso', 'success')
    return redirect(url_for('routes.index'))


@bp.route('/users/admin/<user_id>', methods=['POST'])
def users_admin(user_id):
    user_obj = users.find_one(user_id)
    user_obj['admin'] = True
    users.update(user_id, user_obj)
    return redirect(url_for('routes.users_index'))
