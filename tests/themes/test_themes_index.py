from flask import url_for


def test_login_required(client):
    response = client.get(url_for('routes.themes_index'), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_logged_user):
    response = client.get(url_for('routes.themes_index'), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_logged_user):
    response = client.post(url_for('routes.themes_index'), follow_redirects=True)
    assert response.status_code == 405


def test_success_list_data(client, test_theme, test_logged_user):
    response = client.get(url_for('routes.themes_index'), follow_redirects=True)
    assert str.encode(test_theme['name']) in response.data
    assert str.encode(test_theme['last_update_date']) in response.data
