import os
from flask import Flask
from flask_pymongo import PyMongo

mongo = PyMongo()

def create_app():
    app = Flask(__name__)
    app.config.from_pyfile('config.py')
    mongo.init_app(app)

    try:
        # os.makedirs(app.instance_path)
        os.makedirs(os.path.join(app.instance_path, 'storage'), exist_ok=True)
        app.config['UPLOAD_FOLDER'] = os.path.join(app.instance_path, 'storage')
        app.config['CORS_HEADERS'] = 'Content-Type'
    except OSError:
        pass

    configure_blueprints(app)

    return app

def configure_blueprints(app):
    from api.routes import bp as route_bp
    from api.dbs import bp as dbs_bp
    app.register_blueprint(route_bp)
    app.register_blueprint(dbs_bp)

app = create_app()

if __name__ == '__main__':
    app.run(ssl_context='adhoc')