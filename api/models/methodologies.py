from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SelectMultipleField, widgets, RadioField, DateField, IntegerField
from wtforms.validators import DataRequired


class MethodologiesForm(FlaskForm):

    name = StringField('Nome', validators=[DataRequired()])
    text = TextAreaField('Texto', validators=[DataRequired()])
    authors = SelectMultipleField('Autores', validators=[DataRequired()])
    thermometer = IntegerField('Termômetro', validators=[DataRequired()])
    last_update_date = DateField()

