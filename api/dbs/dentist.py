from api import mongo
from bson.objectid import ObjectId
import pymongo

collection = mongo.db.dentists_col

def find_by_facebook_id(facebook_id):
    return collection.find_one({'facebook_id': facebook_id})

def find_by_google_id(google_id):
    return collection.find_one({'google_id': google_id})

def insert_dentist(obj_dentist):
    collection.insert_one(obj_dentist)

def find_by_email_and_social(email):
    return collection.find_one({'email': email, 'facebook_id': '', 'google_id':''}) 

def find_by_email(email):
    return collection.find_one({'email': email})