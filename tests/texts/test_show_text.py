from flask import url_for, json

headers = {
    'Content-Type': 'application/json'
}
fakeId = '5dc483761ded37ccee5c7f90'

def test_valid_key_data(client, app, test_text, test_methodology):
    valid_key_data = dict(key=app.config['SECRET_KEY'], _id=str(test_text['_id']), dentist_id=fakeId)
    response = client.post(url_for('routes.show_text'), data=json.dumps(valid_key_data), headers=headers)
    assert response.status_code == 200
    returned_obj = json.loads(response.data)
    assert returned_obj['status'] == 200
    assert returned_obj['text']['title'] == test_text['title']
    assert returned_obj['text']['thermometer'] == test_methodology['thermometer']
    assert returned_obj['text']['likestatus'] is None

def test_valid_key_data_with_userlike(client, app, test_text, like_test):
    valid_key_data = dict(key=app.config['SECRET_KEY'], _id=str(test_text['_id']),
                          dentist_id=str(like_test['dentist_id']))
    response = client.post(url_for('routes.show_text'), data=json.dumps(valid_key_data), headers=headers)
    assert response.status_code == 200
    returned_obj = json.loads(response.data)
    assert returned_obj['status'] == 200
    assert returned_obj['text']['likestatus'] == 'like'

def test_valid_key_data_with_userdislike(client, app, test_text, dislike_test):
    valid_key_data = dict(key=app.config['SECRET_KEY'], _id=str(test_text['_id']),
                          dentist_id=str(dislike_test['dentist_id']))
    response = client.post(url_for('routes.show_text'), data=json.dumps(valid_key_data), headers=headers)
    assert response.status_code == 200
    returned_obj = json.loads(response.data)
    assert returned_obj['status'] == 200
    assert returned_obj['text']['likestatus'] == 'dislike'


def test_receive_likes(client, app, test_text, like_test, another_dislike_test):
    valid_key_data = dict(key=app.config['SECRET_KEY'], _id=str(test_text['_id']), dentist_id=fakeId)
    response = client.post(url_for('routes.show_text'), data=json.dumps(valid_key_data), headers=headers)
    assert response.status_code == 200
    returned_obj = json.loads(response.data)
    assert returned_obj['status'] == 200
    assert returned_obj['text']['likes'] == 1
    assert returned_obj['text']['dislikes'] == 1


def test_invalid_key_data(client, app, test_text):
    invalid_key_data = dict(key=app.config['SECRET_KEY'] + 'x', _id=str(test_text['_id']), dentist_id=fakeId)
    response = client.post(url_for('routes.show_text'), data=json.dumps(invalid_key_data), headers=headers)
    assert response.status_code == 200
    returned_obj = json.loads(response.data)
    assert returned_obj['status'] == 401
