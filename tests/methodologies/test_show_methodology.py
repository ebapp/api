from flask import url_for, json

headers = {
    'Content-Type': 'application/json'
}


def test_valid_key_data(client, test_methodology, app):
    data = dict(key=app.config['SECRET_KEY'], _id=str(test_methodology['_id']))
    response = client.post(url_for('routes.show_methodology'), data=json.dumps(data), headers=headers)
    assert response.status_code == 200
    returned_obj = json.loads(response.data)
    assert returned_obj['status'] == 200


def test_invalid_key_data(client, test_methodology, app):
    data = dict(key=app.config['SECRET_KEY'] + 'x', _id=str(test_methodology['_id']))
    response = client.post(url_for('routes.show_methodology'), data=json.dumps(data), headers=headers)
    assert response.status_code == 200
    returned_obj = json.loads(response.data)
    assert returned_obj['status'] == 401
