from flask import url_for
import pytest
from io import BytesIO

@pytest.fixture
def data():
    return dict(description='test_description',
                url_image=(BytesIO(b'test_data'), 'test_data.jpg'),
                order_no=5)

def test_login_required(client,test_text):
    response = client.get(url_for('routes.slide_create',text_id=str(test_text['_id'])), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_logged_user,test_text):
    response = client.get(url_for('routes.slide_create',text_id=str(test_text['_id'])), follow_redirects=True)
    assert response.status_code == 200

def test_post_response(client, test_logged_user, collection, data, test_text):
    response = client.post(url_for('routes.slide_create',text_id=str(test_text['_id'])), data=data, follow_redirects=True)
    assert response.status_code == 200
    collection.slideshow_col.delete_many({'description': data['description']})


def test_valid_slideshow_data_redirection(client, test_logged_user, collection, data, test_text):
    response = client.post(url_for('routes.slide_create',text_id=str(test_text['_id'])), data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'success' in response.data
    collection.slideshow_col.delete_many({'description': data['description']})


def test_create_object(client, test_logged_user, collection, data, test_text):
    print(data)
    client.post(url_for('routes.slide_create',text_id = str(test_text['_id'])), data=data, follow_redirects=True)
    slideshow_obj = collection.slideshow_col.find_one({'description': data['description']})
    assert slideshow_obj['description'] == data['description']
    assert slideshow_obj['url_image']
    assert slideshow_obj['order_no'] == data['order_no']
    collection.slideshow_col.delete_many({'description': data['description']})

def test_auto_increment_slide_order(client, collection, test_logged_user, test_text):
    for i in range(5):
        slide = dict(description='test_description',
                url_image='test_data.jpg',
                order_no=i,
                text_id = test_text['_id'])
        collection.slideshow_col.insert_one(slide)
    response = client.get(url_for('routes.slide_create',text_id=str(test_text['_id'])), follow_redirects=True)
    assert b'<input class="form-control-file" id="order_no" name="order_no" required type="text" value="6">' in response.data

def test_auto_increment_slide_first(client, test_logged_user, test_text):
    response = client.get(url_for('routes.slide_create',text_id=str(test_text['_id'])), follow_redirects=True)
    assert b'<input class="form-control-file" id="order_no" name="order_no" required type="text" value="1">' in response.data