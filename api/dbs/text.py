from api import mongo
import pymongo
from bson.objectid import ObjectId

collection = mongo.db.texts_col 

# insert text to database
def insert(obj):
    collection.insert_one(obj)

# update text from database
def update(text_id, obj):
    collection.find_one_and_update({'_id': ObjectId(text_id)}, {'$set': obj})

# delete text from database
def delete(text_id):
    collection.delete_one({'_id': ObjectId(text_id)})

def find_all():
    return list(collection.find().sort("last_update_date", pymongo.DESCENDING))

def find_one(text_id):
    text = collection.find_one({'_id': ObjectId(text_id)})
    return text

def find(theme, keyword):
    if theme:
        texts = list(collection.find({'$and': [{'themes': theme}, {'$or': [{'title': {"$regex": keyword, "$options": "i"}}, {'abstract': {"$regex": keyword, "$options": "i"}}]}]}, {"title": 1, "url_image": 1}))
    else:
        texts = list(collection.find({'$or': [{'title': {"$regex": keyword, "$options": "i"}}, {'abstract': {"$regex": keyword, "$options": "i"}}]}, {"title": 1, "url_image": 1}))
    return texts
