# About
This programming interface is responsible for providing, receiving and maintaining data from the Evident.
The technologies used in this repository are based in the Python programming language (including the Flask framework), and the Mongo database.
 Feel free to contribute with us by following our <a href="https://gitlab.com/eba.pp/api/blob/master/CONTRIBUTING.md">CONTRIBUTING GUIDELINES</a>.

This project is comprised of two key components: the API and the mobile application. This repository contains the API, which was developed using the Python Programming Language. Flask is a Python Library, whose purpose is to -. It is worth mentioning that the API is crucial for the mobile application to function properly. Furthermore, the API

# Installation
If you are interested in contributing with our interface, the first step is to install the following technologies:

<b>Python 3.5 (Programming Language):</b>
><b>Linux:</b> https://docs.python.org/3/using/unix.html <br>
><b>Windows:</b> https://docs.python.org/3/using/windows.html <br>
><b>Macintosh:</b> https://docs.python.org/3/using/mac.html <br>

<b>MongoDB 4.0 (NoSQL Database):</b>
><b>Linux:</b> https://docs.mongodb.com/manual/administration/install-on-linux/ <br>
><b>Windows:</b> https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/ <br>
><b>Macintosh:</b> https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/ <br>

<b>PIP (Package Manegement System):</b>
><b>All the platforms:</b> https://pip.pypa.io/en/stable/installing/ <br>

# Project Layout

The project directory contain:

* **api/**, a Python package containing your application code and files.
* **tests/**, a directory containing test modules.
* **venv/**, a Python virtual environment where Flask and other dependencies are installed.

# Execute the API


## Create a virtual environment
> virtualenv venv

## Activate the corresponding environment
> . venv/bin/activate

## Install dependencies

> pip install -r requirements.txt

## Run the API

Make sure MongoDB is up.

> export FLASK_APP=api

> flask run

## Run Tests

> pytest

or

> pytest path/to/file

## Deployment server 

Ask project owners for an account at `Nuvem USP` and reset the password of the VM in the interface.

* Login at https://internuvem.usp.br
* Instances > Evident > Reset Password (key icon)

```
ssh ubuntu@evident.gq
```
