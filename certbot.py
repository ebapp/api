from flask import Flask, request, redirect

app = Flask(__name__)

@app.route('/.well-known/acme-challenge/<path:token>')
def hello(token):
    f = open('/home/ubuntu/webroot/.well-known/acme-challenge/' + token)
    text = f.read()
    f.close()
    return text

app.run(host='0.0.0.0', port=80)
