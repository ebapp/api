from api.routes import bp
from flask import request, jsonify, current_app as app
from api.dbs import dentist
from datetime import datetime
from api.models.dentists import DentistsForm
from flask_cors import cross_origin
import bcrypt

@bp.route('/dentists/', methods=['POST'])
@cross_origin()
def get_dentist():
    dentist_obj = None
    request_data = request.get_json()

    if request_data['key'] != app.config['SECRET_KEY']:
        return jsonify({'error': 'unauthorized'})

    if 'facebook_id' in request_data:
        dentist_obj = dentist.find_by_facebook_id(request_data['facebook_id'])
    elif 'google_id' in request_data:
        dentist_obj = dentist.find_by_google_id(request_data['google_id'])

    if dentist_obj == None:
        return jsonify({'error': 'not found'})
    else:
        dentist_obj['_id'] = str(dentist_obj['_id'])
        return jsonify(dentist_obj)


@bp.route('/dentists/create', methods=['POST'])
@cross_origin()
def dentists_create():
    form = DentistsForm()

    if form.key.data != app.config['SECRET_KEY']:
        return jsonify({'error': 'unauthorized'})

    dentist_obj = dentist.find_by_email(form.email.data)
    if dentist_obj != None:
        return jsonify({'erro': 'Um usuário com este email já esta cadastrado.'})
    if form.validate_on_submit():
        email_password = bcrypt.hashpw((form.email_password.data).encode('UTF-8'), bcrypt.gensalt())    
        
        obj = {
            'facebook_id': form.facebook_id.data,
            'google_id': form.google_id.data,
            'name': form.name.data,
            'email': form.email.data,
            'email_password' : email_password.decode('UTF-8'),
            'birth_year': form.birth_year.data,
            'gender': form.gender.data,
            'grade_year': form.grade_year.data,
            'grade_state': form.grade_state.data,
            'last_update_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'occupation': form.occupation.data,
            'work_state': form.work_state.data,
            'app_ad': form.app_ad.data

        }
        dentist.insert_dentist(obj)
        obj['_id'] = str(obj['_id'])
        return jsonify(obj)
    return jsonify({'erro': 'Favor preencher todos os campos.',
                    'form_error': form.errors})

@bp.route('/dentists/login_email', methods=['POST'])
@cross_origin()
def dentists_login_email():
    request_data = request.get_json()

    if request_data['key'] != app.config['SECRET_KEY']:
        return jsonify({'error': 'unauthorized'})

    email = request_data['email']
    email_password = request_data['email_password']

    if email != '' and email_password != '':
        dentist_obj = dentist.find_by_email_and_social(request_data['email'])
        if dentist_obj == None:
            return jsonify({'error': 'Usuário não encontrado'})

        email_password = bytes(dentist_obj['email_password'], 'UTF-8')
        email_password_requision = bytes(request_data['email_password'], 'UTF-8')

        if bcrypt.checkpw(email_password_requision, email_password):
            dentist_obj['email_password'] = ''
            dentist_obj['_id'] = str(dentist_obj['_id'])
            return jsonify(dentist_obj)
        else:
            return jsonify({'error': 'Senha inválida'})

    return jsonify({'error': 'Favor informar os dados!'})  
