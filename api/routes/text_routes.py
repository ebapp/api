from flask import redirect, render_template, url_for, request, flash, send_from_directory, current_app as app, jsonify
from datetime import datetime
from bson import json_util
import json
import os
from api.routes import bp
from api.dbs import text, methodology, themes, users, likes
from api.models.texts import TextsForm
from api.routes.user_routes import login_required
from api import handlers
from werkzeug.utils import secure_filename
from flask_cors import cross_origin
from flask_wtf.file import FileRequired


@bp.route("/text/detail", methods=['POST'])
@cross_origin()
def show_text():
    response = {}
    content = request.json

    if content['key'] != app.config['SECRET_KEY']:
        handlers.errorUnauthorized('text', response)
        return jsonify(response)

    text_detail = text.find_one(content['_id'])

    if not handlers.successObjectReturned('text', text_detail, response):
        handlers.successNoContent('text', response)
        return jsonify(response)

    response['status'] = 200
    response['text'] = text_detail
    response['text']['likes'] = likes.count_likes(content['_id'])
    response['text']['dislikes'] = likes.count_dislikes(content['_id'])
    like = likes.find_one(content['_id'], content['dentist_id'])
    response['text']['likestatus'] = None if like is None else like['state']
    if len(text_detail['methodologies']) > 0:
        the_only = text_detail['methodologies'][0]
        response['text']['thermometer'] = methodology.find_one(the_only)['thermometer']

            

    json_response = json.dumps(response, default=json_util.default, ensure_ascii=False).encode('utf8')
    return json_response


@bp.route("/text/search", methods=['POST'])
@cross_origin()
def search_texts():
    if request.method == 'POST':
        response = {}
        content = request.json

        if content['key'] != app.config['SECRET_KEY']:
            handlers.errorUnauthorized('texts', response)
        else:
            theme = content['theme']
            keyword = content['keyword']
            texts = text.find(theme, keyword)

            if not handlers.successObjectReturned('texts', texts, response):
                handlers.successNoContent('texts', response)

        json_response = json.dumps(response, default=json_util.default, ensure_ascii=False).encode('utf8')
        return json_response


@bp.route('/texts/index', methods=['GET'])
@login_required
def texts_index():
    texts = text.find_all()
    return render_template('texts/index.html', texts=texts)


@bp.route('/texts/create', methods=['GET', 'POST'])
@login_required
def texts_create():
    all_methodologies = methodology.find_all()
    all_themes = themes.find_all()
    all_authors = users.find_all()
    form = TextsForm()
    form.url_image.validators.append(FileRequired())
    form.methodologies.choices = [(str(methodology_obj['_id']), methodology_obj['name']) for methodology_obj in
                                  all_methodologies]
    form.themes.choices = [(str(theme_obj['_id']), theme_obj['name']) for theme_obj in all_themes]
    form.abstract_authors.choices = [(str(author_obj['_id']), author_obj['name']) for author_obj in all_authors]

    if form.validate_on_submit():
        f = form.url_image.data
        filename = secure_filename(form.title.data + f.filename)
        f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        obj = {
            'title': form.title.data,
            'url_image': url_for('routes.storage', filename=filename, _external=True),
            'abstract_intro': form.abstract_intro.data,
            'abstract': form.abstract.data,
            'methodologies': form.methodologies.data,
            'themes': form.themes.data,
            'paper_link': form.paper_link.data,
            'abstract_authors': form.abstract_authors.data,
            'study_relevance': form.study_relevance.data,
            'posting_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'last_update_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        }
        text.insert(obj)
        flash('Texto criado com sucesso', 'success')
        return redirect(url_for('routes.texts_index'))
    if 'title' in form.errors:
        flash('Adicione um titulo', 'danger')        
    if 'url_image' in form.errors:
        flash('Adicione uma imagem', 'danger')
    if 'themes' in form.errors:
        flash('Adicione um tema', 'danger')
    if 'methodologies' in form.errors:
        flash('Adicione uma metodologia', 'danger')
    if 'paper_link' in form.errors:
        flash('Adicione uma url para o artigo ', 'danger')
    if 'abstract_authors' in form.errors:
        flash('Adicione um autor', 'danger')    
    if 'study_relevance' in form.errors:
        flash('Adicione uma evidencia', 'danger')
    return render_template('texts/form.html', title_label='Criar Texto', form=form, button_label='Criar')


@bp.route('/texts/read/<text_id>', methods=['GET'])
@login_required
def texts_read(text_id):
    text_obj = text.find_one(text_id)
    authors_list = users.find_selected(text_obj['abstract_authors'])
    themes_list = themes.find_selected(text_obj['themes'])
    methodologies_list = methodology.find_selected(text_obj['methodologies'])
    return render_template('texts/read.html', text=text_obj, authors=authors_list, themes=themes_list,
                           methodologies=methodologies_list)


@bp.route('/texts/update/<text_id>', methods=['GET', 'POST'])
@login_required
def texts_update(text_id):
    all_methodologies = methodology.find_all()
    all_themes = themes.find_all()
    all_authors = users.find_all()
    text_obj = text.find_one(text_id)
    form = TextsForm(data=text_obj)
    form.methodologies.choices = [(str(methodology_obj['_id']), methodology_obj['name']) for methodology_obj in
                                  all_methodologies]
    form.themes.choices = [(str(theme_obj['_id']), theme_obj['name']) for theme_obj in all_themes]
    form.abstract_authors.choices = [(str(author_obj['_id']), author_obj['name']) for author_obj in all_authors]

    if form.validate_on_submit():
        if 'url_image' in request.files:
            f = form.url_image.data
            filename = secure_filename(form.title.data + f.filename)
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            text_obj['url_image'] = url_for('routes.storage', filename=filename, _external=True)
        text_obj['title'] = form.title.data
        text_obj['abstract_intro'] = form.abstract_intro.data
        text_obj['abstract'] = form.abstract.data
        text_obj['methodologies'] = form.methodologies.data
        text_obj['themes'] = form.themes.data
        text_obj['paper_link'] = form.paper_link.data
        text_obj['abstract_authors'] = form.abstract_authors.data
        text_obj['study_relevance'] = form.study_relevance.data
        text_obj['last_update_date'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        text.update(text_id, text_obj)
        flash('Texto atualizado com sucesso', 'success')
        return redirect(url_for('routes.texts_index'))
    return render_template('texts/form.html', title_label='Atualizar Texto', form=form, button_label='Atualizar')


@bp.route('/texts/delete/<text_id>', methods=['POST'])
@login_required
def texts_delete(text_id):
    text.delete(text_id)
    flash('Texto removido com sucesso', 'success')
    return redirect(url_for('routes.texts_index'))


@bp.route('/texts/storage/<filename>', methods=['GET', 'POST'])
@cross_origin()
def storage(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

@bp.route("/text/like", methods=['POST'])
@cross_origin()
def like():
    request_data = request.get_json()

    if request_data['key'] != app.config['SECRET_KEY']:
        return jsonify({'error': 'unauthorized'})

    if not request_data['state'] in ['like', 'dislike', 'remove']:
        return jsonify({'error': 'state must be like, dislike or remove'})

    if request_data['state'] == 'remove':
        likes.delete_one(request_data['text_id'], request_data['dentist_id'])
    else:
        likes.update_one(request_data['text_id'], request_data['dentist_id'], request_data['state'])
    qtd_likes = likes.count_likes(request_data['text_id'])
    qtd_dislikes = likes.count_dislikes(request_data['text_id'])

    return jsonify({'status': request_data['state'], 'likes': qtd_likes, 'dislikes': qtd_dislikes})
