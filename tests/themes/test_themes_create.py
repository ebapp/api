from flask import url_for


def test_login_required(client):
    response = client.get(url_for('routes.themes_create'), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_logged_user):
    response = client.get(url_for('routes.themes_create'), follow_redirects=True)
    assert response.status_code == 200


def test_post_response(client, test_logged_user):
    response = client.post(url_for('routes.themes_create'), follow_redirects=True)
    assert response.status_code == 200


def test_valid_themes_data_redirection(client, test_logged_user):
    data = dict(name='test_theme',
                color='#57A319')
    response = client.post(url_for('routes.themes_create'), data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'success' in response.data


def test_invalid_themes_data_redirection(client, test_logged_user):
    data = dict(name='test_theme',
                color='#000000')
    response = client.post(url_for('routes.themes_create'), data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'success' not in response.data


def test_create_object(client, test_logged_user, collection):
    data = dict(name='test_create_theme',
                color='#57A319')
    client.post(url_for('routes.themes_create'), data=data, follow_redirects=True)
    theme_obj = collection.themes_col.find_one({'name': 'test_create_theme'})
    assert theme_obj['name'] == data['name']
    assert theme_obj['color'] == data['color']
