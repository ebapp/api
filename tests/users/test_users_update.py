from flask import url_for
from bson.objectid import ObjectId

valid_user_obj = {
    'name': 'edited_test_user',
    'email': 'test@test.com',
    'password': 'edited_test_password',
    'confirm': 'edited_test_password',
    'about': 'edited_about_test_user'
}

invalid_user_obj = {
    'name': 'test_user',
    'email': 'test@test.com',
    'password': 'test_password',
    'confirm': 'wrong_password',
    'about': 'about_test_user'
}


def test_login_required(client, collection, test_user):
    client.post(url_for('routes.users_update', user_id=test_user['_id']), data=valid_user_obj, follow_redirects=True)
    edited_user = collection.users_col.find_one({'_id': ObjectId(test_user['_id'])})
    assert not edited_user['name'] == 'edited_test_user'


def test_get_response(client, test_user, test_admin_user):
    response = client.get(url_for('routes.users_update', user_id=test_user['_id']))
    assert response.status_code == 200


def test_post_response(client, test_user, test_admin_user):
    response = client.post(url_for('routes.users_update', user_id=test_user['_id']), data=valid_user_obj,
                           follow_redirects=True)
    assert response.status_code == 200


def test_success_update_data(client, test_user, test_admin_user, collection):
    client.post(url_for('routes.users_update', user_id=test_user['_id']), data=valid_user_obj, follow_redirects=True)
    edited_user = collection.users_col.find_one({'_id': ObjectId(test_user['_id'])})
    assert edited_user['name'] == 'edited_test_user'
    assert edited_user['about'] == 'edited_about_test_user'
