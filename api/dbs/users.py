from api import mongo
from bson.objectid import ObjectId

collection = mongo.db.users_col


def insert(obj):
    collection.insert_one(obj)


def update(user_id, obj):
    collection.find_one_and_update({'_id': ObjectId(user_id)}, {'$set': obj})


def delete(user_id):
    collection.delete_one({'_id': ObjectId(user_id)})


def find_all():
    return list(collection.find())


def find_one(user_id):
    user = collection.find_one({'_id': ObjectId(user_id)})
    return user


def find_selected(user_list):
    return collection.find({'_id': {'$in': [ObjectId(user_id) for user_id in user_list]}})


def find_by_email(user_email):
    return collection.find_one({'email': user_email})
