from flask import url_for


def test_login_required(client, test_text):
    response = client.post(url_for('routes.texts_delete', text_id=str(test_text['_id'])), follow_redirects=True)
    assert b'danger' in response.data


def test_get_response(client, test_text, test_logged_user):
    response = client.get(url_for('routes.texts_delete', text_id=str(test_text['_id'])), follow_redirects=True)
    assert response.status_code == 405


def test_post_response(client, test_text, test_logged_user):
    response = client.post(url_for('routes.texts_delete', text_id=str(test_text['_id'])), follow_redirects=True)
    assert response.status_code == 200


def test_success_delete_data(client, test_text, test_logged_user, collection):
    response = client.post(url_for('routes.texts_delete', text_id=str(test_text['_id'])), follow_redirects=True)
    text_obj = collection.texts_col.find_one(test_text)
    assert b'success' in response.data
    assert not text_obj
