from flask import url_for, session


def test_get_response(client, test_logged_user):
    response = client.get(url_for('routes.users_logout'), follow_redirects=True)
    assert response.status_code == 200


def test_session_clear(client, test_logged_user):
    client.get(url_for('routes.users_logout'), follow_redirects=True)
    assert session.get('user') is None
